
# ml_data

### Subfolder containing datasets used to train and test machine learning. 


| Folder | Description | 
| --- | --- | 
| ml_feature_sets | Contains text files which contain a list of feature names or groups. The files in this folder are designed to be used as input for the -feature or the -feat_group argument of the machine learning pipleine (the ML_Classification script). These kind of files are often produced as output from performing feature selection (Feature_selection.py script) or as a result of grouping together features into groups (the feature_grouping_for_lofo.R script) |
| ml_test_sets | Contains text files which contain a list of eQTLs (snp ids and gene ids in the form rsid_ensembl-id) that are allocated to the test set so they aren't used during model building - but model performance is tested on them, designed to be input for -test argument of the machline learning pipeline. | 
| ml_full_sets | Contains complete data frames of eQTLs and their features (.txt files with row names, sep = "\t"). These files are either designed to be passed to the ML_preprocess.py step if not yet preprocessed, or ML_classification.py step if already preprocessed. Columns are features, rows are gene / snp pairs. Row ids are in the form rsid_ensembl-id. | 

