
## gene_cis_regions

Contains the files telling tabix and Plink what to parse for 10000 genomes VCF filtering (for more information, and to see how these files were created, see the workflowr page: set_up_1000_genomes_filt).

Subfolders by chromosome number, number 23 refers to chromosome X. 

In each subfolder, there is one file labelled: cis_region_positions.txt (of cis-regions for tabix to parse), and a number of files in the form rsid_list_{ensembl_gene_id}.txt - this is a list of test rsids for each ensembl gene - to provide to Plink. 

