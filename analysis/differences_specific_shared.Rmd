---
title: "Investigation into the dstrbution of features across specifc and shared eQTLs"
output:
  workflowr::wflow_html:
    toc: true
editor_options:
  chunk_output_type: console
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Set up 

```{r setup_2, message = FALSE, warning=FALSE,collapse=TRUE}

library(ggplot2)
library(dplyr)
library(data.table)
library(ggpubr)
library(reactable)
library(patchwork)

# Set up custom theme for ggplots
custom_theme <-
  list(
    theme_bw() +
      theme(
        panel.border = element_blank(),
        axis.line = element_line(),
        text = element_text(size = 9),
        legend.position = "bottom",
        strip.background = element_blank(),
        axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)
      ) 
  )

# eQTLs significant in indonesian pop.  lsfr < 0.01
# specific if lsfr > 0.1 in secondary populations
# from usng mashr - 
#indo_spec = read.table("output/ml_data/natri_indo_eqtls_lsfr0.01.txt", sep = "\t")

indo_spec = read.table("output/ml_data/natri_indo_lfsr0.01_strict_shared_features_df.txt", sep = "\t")

# add binary column of population specific and shared 
indo_spec = indo_spec %>% 
            tibble::rownames_to_column("gene_snp") %>% 
            rowwise() %>% 
            mutate(class_binary = ifelse(Class == "shared", 0, 1)) %>% 
            ungroup()


# eQTLs significant in euro pops lsfr < 0.01
euro_spec = read.table("output/ml_data/natri_euro_lfsr0.01_features_df_update.txt", sep = "\t")

euro_spec = euro_spec %>% 
            tibble::rownames_to_column("gene_snp") %>% 
            rowwise() %>% 
            mutate(class_binary = ifelse(Class == "shared", 0, 1)) %>% 
            ungroup()


# permutation test of means

permutation_mean_test <- function(feature, # feature vector
                             class, #class binary column vector
                             n){ # number of permutations to perform
  
  
# Adapting permutation test code from
# https://thomasleeper.com/Rcourse/Tutorials/permutationtests.html
# and https://towardsdatascience.com/permutation-test-in-r-77d551a9f891  
  
# Step 1: Calculate difference in means (this is the test statistics in the difference in means)  

test_stat =   diff(
                   by(feature,
                   class, 
                   function(x) mean(x, na.rm = T)
                                                  )
                                                    )

require(parallel)

cl = parallel::makeCluster(future::availableCores())

parallel::clusterSetRNGStream(cl)

# Step 2: Calculate difference in means for n permutations
# This is the distribution of the test statistics  

# Do this in parallel: 

dist_stats = parallel::parSapply(cl, 1:n, function(i,...) { diff(
                                          by(feature ,
                                          sample(class, length(class)), 
                                                function(x) mean(x, na.rm = T)
                                                                               )) } )


# 
# dist_stats =   replicate(n, 
#                         diff(
#                       by(feature ,
#                       sample(class, length(class)), 
#                       function(x) mean(x, na.rm = T)
#                                                   )))

parallel::stopCluster(cl)
                                     

# Step 3: Perform a two tailed
# Compare the test statistic with the distribution of the test statistic

two_tailed_test = sum(abs(dist_stats) > abs(test_stat))/n

return(two_tailed_test)  

  
}  

```

# eQTL summary statistics

```{r eqtl_summary_statistics}

indo_spec %>% 
  group_by(Class) %>% 
  summarise(mean_effect_size = mean(indo_beta, na.rm = T))

indo_spec %>% 
   group_by(Class) %>% 
   summarise(mean_effect_size = mean(abs(indo_beta), na.rm = T))

indo_spec %>% 
   ggviolin(y="indo_beta", 
           x = "Class",
           fill = "Class",
           add = "boxplot") + 
  labs(y= "Effect size",
       title = "Effect Size in Indonesian Sample") 

indo_spec %>% 
  mutate(abs_indo_beta = abs(indo_beta)) %>% 
   ggviolin(y="abs_indo_beta", 
           x = "Class",
           fill = "Class",
           add = "boxplot") + 
  labs(y= "Absolute Effect Size",
       title = "Absolute Effect Size in Indonesian Sample") 

permutation_mean_test(indo_spec$indo_beta,
                 indo_spec$class_binary,
                 10^6)

permutation_mean_test(abs(indo_spec$indo_beta),
                 indo_spec$class_binary,
                 10^6)

```


# Allele frequency 

```{r allele_freq}

indo_spec %>% 
  group_by(Class) %>% 
  summarise(across(contains("AF")|contains("af"), ~mean(.x, na.rm = T)))

indo_spec %>% 
   ggviolin(y="gnomAD_AF_nfe", 
           x = "Class",
           fill = "Class",
           add = "boxplot") + 
  labs(y= "Allele Frequency",
       title = "gnomAD Allele Frequency in the Non-Finnish European population") 


permutation_mean_test(indo_spec$gnomAD_AF_nfe,
                 indo_spec$class_binary,
                 10^6)

indo_spec %>% 
   ggviolin(y="gnomAD_AF", 
           x = "Class",
           fill = "Class",
           add = "boxplot") + 
   labs(y= "Allele Frequency",
        title = "gnomAD Entire Dataset Allele Frequency") 


permutation_mean_test(indo_spec$gnomAD_AF,
                 indo_spec$class_binary,
                 10^6)


indo_spec %>% 
   ggviolin(y="gnomAD_AF_popmax", 
           x = "Class",
           fill = "Class",
           add = "boxplot") + 
   labs(y= "Maximum Allele Frequency in gnomAD Populations") 

permutation_mean_test(indo_spec$gnomAD_AF_popmax,
                 indo_spec$class_binary,
                 10^6)


indo_spec %>% 
   ggviolin(y="thousand_EUR_AF", 
           x = "Class",
           fill = "Class",
           add = "boxplot") + 
   labs(y= "1000 Genomes Allele Frequency European") 


permutation_mean_test(indo_spec$thousand_EUR_AF,
                 indo_spec$class_binary,
                 10^6)

indo_spec %>% 
   ggviolin(y="thousand_AF", 
           x = "Class",
           fill = "Class",
           add = "boxplot") + 
labs(y= "1000 Genomes Dataset Allele Frequency")

permutation_mean_test(indo_spec$thousand_AF,
                 indo_spec$class_binary,
                 10^6)

```

# Gene constraint and conservation

```{r constraint_conservation}
# comparing mean gene conservation and constraint scores 
# across population specific and shared eQTLs

indo_spec %>% 
   group_by(Class) %>% 
   summarise(mean_phastcons = mean(phastcons, na.rm = T),
             mean_oe_syn_lower = mean(oe_syn_lower, na.rm = T),
             mean_oe_syn_upper = mean(oe_syn_upper, na.rm = T),
             mean_loeuf = mean(oe_lof_upper, na.rm = T)) %>% 
  mutate(across(!Class, ~round(.x, digits = 2))) %>% 
  reactable()


euro_spec %>% 
   group_by(Class) %>% 
   summarise(mean_phastcons = mean(phastcons, na.rm = T),
             mean_oe_syn_lower = mean(oe_syn_lower, na.rm = T),
             mean_oe_syn_upper = mean(oe_syn_upper, na.rm = T),
             mean_loeuf = mean(oe_lof_upper, na.rm = T)) %>% 
  mutate(across(!Class, ~round(.x, digits = 2))) %>% 
  reactable()

```

## gnomAD constraint scores

### LOEUF

```{r loeuf}

# distribution of LOEUF scores
indo_spec %>% 
  ggviolin(y="oe_lof_upper", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "LOEUF") + 
euro_spec %>%   
  ggviolin(y="oe_lof_upper", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "LOEUF") + 
  plot_annotation(title = "LOEUF",
                  subtitle = "Upper bound on 90% confidence interval of observed over expected loss of function mutations",
                  caption = "Lower values indicate higher levels of constraint")


# Significance of these distributional differences:

# permutation significance test : 

# Indonesian dataset
permutation_mean_test(indo_spec$oe_lof_upper, 
                 indo_spec$class_binary, 
                 10^6)

# European dataset
permutation_mean_test(euro_spec$oe_lof_upper, 
                     euro_spec$class_binary, 
                     10^6)


```

## Synonmous constaint

```{r gnomad_synp}

# distribution of oe_syn_upper scores 
(indo_spec %>% 
  ggviolin(y="oe_syn_upper", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "Upper bound of the 90% confidence interval of observed over expected") + 
euro_spec %>% 
  ggviolin(y="oe_syn_upper", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "") )+ 
plot_annotation(title = "Synomous Constraint (oe syn upper)",
                )  

# 
permutation_mean_test(indo_spec$oe_syn_upper, 
                 indo_spec$class_binary, 
                 10^6)

permutation_mean_test(euro_spec$oe_syn_upper, 
                     euro_spec$class_binary, 
                     10^6)

indo_spec %>% 
  group_by(Class) %>% 
  ggviolin(y="oe_syn_lower", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "oe_syn_lower")

permutation_mean_test(indo_spec$oe_syn_lower, 
                 indo_spec$class_binary, 
                 10^6)

```

### Misscence constraint

```{r miss_constraint}



```

### phastcons

```{r phastcons}

indo_spec %>% 
  group_by(Class) %>% 
  ggviolin(y="phastcons", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "phastcons") + 
euro_spec %>% 
  group_by(Class) %>% 
  ggviolin(y="phastcons", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") 

# Significance of these distributional differences 

permutation_mean_test(indo_spec$phastcons, 
                 indo_spec$class_binary, 
                 10^6)

```

# PhyloP

```{r phylop}



```

# Probability

```{r probability_constraint}


pLI
classic_caf_amr 
```

# Genomic Location of SNPs

```{r genomic_location, eval = FALSE}

indo_spec %>% 
  group_by(Class) %>% 
  summarise(prop_intron = sum(intron)/n(),
            prop_promoter_core = sum(promoterCore)/n(),
            prop_promoter_prox = sum(promoterProx)/n(),
            prop_exon = sum(exon)/n())

permutation_mean_test(indo_spec$intron,
                 indo_spec$class_binary, 
                 10^6)

permutation_mean_test(indo_spec$promoterCore,
                 indo_spec$class_binary, 
                 10^6)

permutation_mean_test(indo_spec$promoterProx,
                 indo_spec$class_binary, 
                 10^6)


```


# Gene Nucleotide percentage

```{r}

indo_spec %>% 
  group_by(Class) %>% 
  summarise(
            mean_GC = mean(percentage_gene_gc_content, na.rm = T))



indo_spec %>% 
  group_by(Class) %>% 
  ggviolin(y="gene_.GC", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "gene_.GC")

```


# Expression metrics

## Within tissue (GTEx)

```{r within_tisse_metrics}

indo_spec %>% 
  group_by(Class) %>% 
  summarise(across(contains("wb"), ~mean(.x, na.rm = T))) %>% 
  mutate(across(!Class, ~round(.x, digits = 2))) %>% 
  reactable()


(
  (indo_spec %>% 
  ggviolin(y="wbmed_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y = "log (1+ tpm)",
       title = "Median Expression") 
                                      )+ 
(
 indo_spec %>% 
  ggviolin(y="wbmean_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y = "log (1+ tpm)",
       title = "Mean Expression")  
                                    ) + 
(
 indo_spec %>% 
  ggviolin(y="wbmode_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y = "log (1+ tpm)",
       title = "Mode of Expression")  
)    
    
  ) /
(
  (
  indo_spec %>% 
  ggviolin(y="wbmax_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "log (1 + tpm)",
       title = "Maximum Expression") ) + 
(
 indo_spec %>% 
  ggviolin(y="wbmin_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "log (1 + tpm)",
       title = "Minimum Expression") 
                                     ) + 
(
 indo_spec %>% 
  ggviolin(y="wbvar_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y = "log (1+ tpm)",
       title = "Variance of Expression")  
)  
 )



permutation_mean_test(indo_spec$wbmax_log_1p_tpm, 
                 indo_spec$class_binary, 
                 10^6)

permutation_mean_test(indo_spec$wbmed_log_1p_tpm, 
                 indo_spec$class_binary, 
                 10^6)

indo_spec %>% 
  ggviolin(y="wbvar_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot")

permutation_mean_test(indo_spec$wbvar_log_1p_tpm,
                 indo_spec$class_binary, 
                 10^6)

indo_spec %>% 
  ggviolin(y="wbmin_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot")

permutation_mean_test(indo_spec$wbmin_log_1p_tpm,
                 indo_spec$class_binary, 
                 10^6)

```

### Indonesian expression levels

```{r}

library(edgeR)
library(limma)

load("data/expression_data/Natri_2020/indoRNA.read_counts.TMM.filtered.Rda")

indo_mean = apply(yFilt$counts,
                  MARGIN = 1,
                  function(x) mean(log(x + 1)))

indo_min =  apply(yFilt$counts,
                MARGIN = 1,
                function(x) min(log(x + 1)))

indo_max =  apply(yFilt$counts,
      MARGIN = 1,
      function(x) max(log(x + 1)))

indo_var = apply(yFilt$counts,
      MARGIN = 1,
      function(x) var(log(x + 1)))


indo_expression = cbind(indo_mean,
                        indo_min,
                        indo_max,
                        indo_var)
 
indo_expression =   indo_expression %>% 
                    as.data.frame() %>% 
                    tibble::rownames_to_column("gene")

indo_exp_gene = inner_join(indo_spec %>% 
                           tidyr::separate("gene_snp", into = c("gene", "snp")),
                           indo_expression,
                           by= "gene")

permutation_mean_test(indo_exp_gene$indo_min,
                 indo_exp_gene$class_binary, 
                 10^6)

(indo_exp_gene %>% 
  ggviolin(y="indo_min", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y = "minimum  log(tmm + 1) ",
       title  = "Indonesian Expression"))/ (
indo_spec %>% 
  ggviolin(y="wbmin_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y = "minimum  log(tpm + 1) ",
       title  = "GTEx Expression")

           )

indo_exp_gene %>% 
  group_by(Class) %>% 
  summarise(across(contains("indo"), ~mean(.x)))

indo_exp_gene %>% 
  ggviolin(y="indo_max", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") 

permutation_mean_test(indo_exp_gene$indo_max,
                 indo_exp_gene$class_binary, 
                 10^6)

indo_exp_gene %>% 
 ggviolin(y="indo_mean", 
           x = "Class", 
           fill = "Class",
           add = "boxplot")  

permutation_mean_test(indo_exp_gene$indo_mean,
                 indo_exp_gene$class_binary, 
                 10^6)

indo_exp_gene %>% 
 ggviolin(y="indo_var", 
           x = "Class", 
           fill = "Class",
           add = "boxplot")  

permutation_mean_test(indo_exp_gene$indo_var,
                 indo_exp_gene$class_binary, 
                 10^6)

```

## Between tissues (GTEx tissues)

```{r}

indo_spec %>% 
  group_by(Class) %>% 
  summarise(across(contains("PC")|contains("factor"), ~mean(.x, na.rm = T))) %>% 
  reactable()

# from running rf model on the 

# found in order of importance; pc17, factor 9, pc7, factor 2, pc18, pc14, pc9 

indo_spec %>% 
 ggviolin(y="PC17", 
           x = "Class", 
           fill = "Class",
           add = "boxplot")


permutation_mean_test(indo_spec$PC17,
                 indo_spec$class_binary, 
                 10^6)


indo_spec %>% 
 ggviolin(y="factor9", 
           x = "Class", 
           fill = "Class",
           add = "boxplot")


permutation_mean_test(indo_spec$factor9,
                 indo_spec$class_binary, 
                 10^6)

indo_spec %>% 
 ggviolin(y="factor2", 
           x = "Class", 
           fill = "Class",
           add = "boxplot")


permutation_mean_test(indo_spec$factor2,
                 indo_spec$class_binary, 
                 10^6)

indo_spec %>% 
 ggviolin(y="PC2", 
           x = "Class", 
           fill = "Class",
           add = "boxplot")

permutation_mean_test(indo_spec$PC2,
                 indo_spec$class_binary, 
                 10^6)


```

# SNP Conservation metrics

```{r}

indo_spec %>% 
  group_by(Class) %>%
  summarise(across(contains("cadd")|"fitCons" |"linsight", ~mean(.x, na.rm = TRUE))) %>%   
  mutate(across(!Class, ~round(.x, digits = 2))) %>% 
  reactable()



indo_spec %>% 
   ggviolin(y="cadd_1", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") 

permutation_mean_test(indo_spec$cadd_1,
                 indo_spec$class_binary, 
                 10^6)

indo_spec %>% 
 ggviolin(y="cadd_2", 
           x = "Class", 
           fill = "Class",
           add = "boxplot")  

permutation_mean_test(indo_spec$cadd_2,
                 indo_spec$class_binary, 
                 10^6)

indo_spec %>% 
 ggviolin(y="cadd_3", 
           x = "Class", 
           fill = "Class",
           add = "boxplot")  

permutation_mean_test(indo_spec$cadd_3,
                 indo_spec$class_binary, 
                 10^6)

indo_spec %>% 
   ggviolin(y="fitCons", 
           x = "Class", 
           fill = "Class",
           add = "boxplot")

permutation_mean_test(indo_spec$fitCons,
                 indo_spec$class_binary, 
                 10^6)

indo_spec %>% 
   ggviolin(y="linsight", 
           x = "Class", 
           fill = "Class",
           add = "boxplot")

permutation_mean_test(indo_spec$linsight,
                 indo_spec$class_binary, 
                 10^6)

```

# DHS

```{r dhs}
indo_spec %>% 
  group_by(Class) %>% 
  summarise(prop_dhs = sum(V242, na.rm = T)/n()) # this may not actually be dhs - need to check feature_extraction_pipeline

permutation_mean_test(indo_spec$V242,
                 indo_spec$class_binary, 
                 10^6)

```


# Distance between SNP and Gene

```{r dist}
indo_spec %>% 
  group_by(Class) %>% 
  summarise(mean_dist = mean(dist_snp_gene, na.rm = TRUE))

permutation_mean_test(indo_spec$dist_snp_gene,
                 indo_spec$class_binary, 
                 10^6)


indo_spec %>% 
  ggviolin(y="dist_snp_gene", 
           x = "Class", 
           fill = "Class",
           add = "boxplot")

```


# GO + Kegg annotations

```{r go_kegg_anno}


indo_spec %>% 
  group_by(Class) %>% 
  summarise(go_prop = mean(is_go_annotated, na.rm = TRUE))

permutation_mean_test(indo_spec$is_go_annotated,
                 indo_spec$class_binary, 
                 10^6)

permutation_mean_test(indo_spec$is_kegg_annotated,
                 indo_spec$class_binary, 
                 10^6)

indo_spec %>% 
  group_by(Class) %>% 
  summarise(kegg_prop = mean(is_kegg_annotated, na.rm = TRUE))

```

# Immunological function

```{r immuno}

indo_spec %>% 
  group_by(Class) %>% 
  summarise(across(contains("immport")|contains("immuno")|contains("innate")|contains("has_immune_go_term")|contains("GO.0002376"), ~mean(.x, na.rm = T))) %>% 
  reactable()

permutation_mean_test(indo_spec$immport_Antigen_Processing_and_Presentation,
                 indo_spec$class_binary, 
                 10^6)

permutation_mean_test(indo_spec$innatedb_annotated,
                 indo_spec$class_binary, 
                 10^6)

permutation_mean_test(indo_spec$immport_TNF_Family_Members,
                     indo_spec$class_binary, 
                     10^6)

permutation_mean_test(indo_spec$immport_BCRSignalingPathway,
                     indo_spec$class_binary, 
                     10^6)

permutation_mean_test(indo_spec$immport_TCRsignalingPathway,
                     indo_spec$class_binary, 
                     10^6)

permutation_mean_test(indo_spec$has_immune_go_term,
                     indo_spec$class_binary, 
                     10^6)


indo_spec %>% 
  ggplot(aes(x=innatedb_annotated)) + 
  geom_histogram() + 
  facet_wrap(~Class) + 
  custom_them

indo_spec %>% 
  group_by(Class) %>% 
  sample_n(200) %>% 
  ggplot(aes(x=innatedb_annotated)) + 
  geom_histogram() + 
  facet_wrap(~Class) + 
  custom_theme


permutation_mean_test(indo_spec$has_immune_go_term,
                 indo_spec$class_binary, 
                 10^6)


# specific immport categories


permutation_mean_test(indo_spec$immport_Antigen_Processing_and_Presentation,
                 indo_spec$class_binary, 
                 10^6)

permutation_mean_test(indo_spec$immport_Antimicrobials,
                 indo_spec$class_binary, 
                 10^6)

permutation_mean_test(indo_spec$immport_Chemokines,
                 indo_spec$class_binary, 
                 10^6)

permutation_mean_test(indo_spec$immport_Chemokine_Receptors,
                 indo_spec$class_binary, 
                 10^6)

permutation_mean_test(indo_spec$immport_Cytokines,
                 indo_spec$class_binary, 
                 10^6)

permutation_mean_test(indo_spec$immport_Cytokine_Receptors,
                 indo_spec$class_binary, 
                 10^6)


```
