---
title: "1000 genomes: vcf downloading + filtering"
output:
  workflowr::wflow_html:
    toc: true
    code_folding: hide
editor_options:
  chunk_output_type: console
---

```{r setup, include=FALSE}

knitr::opts_chunk$set(engine.opts = list(bash = "-l"), echo = TRUE)

```

## Download and process VCFs from 1000 genomes

```{bash, download_vcf_and_filter}

######## Set up ##########

# The modules to load:

# Old spartan module system
#module  gcc/10.3.0 
#module switch gcc/10.2.0 gcc/10.3.0
#module load parallel/20210322 

# New spartan hpc software module system:

module load GCC/11.3.0
module load BCFtools/1.15.1

module load GCCcore/11.3.0
module load parallel/20220722

# Available CPU resources / allocations
# (jobs are used to parallelise across chromosomes, threads are provided to bcftools for compression)

n_threads=12
n_threads=2
n_jobs=6
n_jobs=1

# Set up short hand for directory names

# Directory for storage of files to only be temporarily stored (scratch)
temp_dir=/data/scratch/projects/punim0586/ibeasley/pop_spec_eqtl_ml
thous_genomes_temp_dir=$temp_dir/output/thous_genomes_bcfs
dbSNP_bcf_dir=$temp_dir/output/dbsnp_bcfs

# If not made already, make folder to store bcfs for dbSNP and 1000 genomes for each chromosome 
for chrom_num in {1..22}
    do 
    mkdir -p $thous_genomes_temp_dir/chrom_$chrom_num
    mkdir -p $dbSNP_bcf_dir/chrom_$chrom_num
    done
     
mkdir -p $thous_genomes_temp_dir/chrom_23
mkdir -p $dbSNP_bcf_dir/chrom_X


export temp_dir
export thous_genomes_temp_dir
export dbSNP_bcf_dir

# if not downloaded and filtered already, then download and filter thous genomes GRCh38 

if [ ! -f $thous_genomes_temp_dir/thous_genomes_GRCh38_full_analysis_set_plus_decoy_hla.fa.fai ]; then
   
   (cd $thous_genomes_temp_dir && \
 wget -q -O - "https://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/reference/GRCh38_reference_genome/GRCh38_full_analysis_set_plus_decoy_hla.fa.fai" |\
 sed -n 's/chr//p' |\
 grep -v -e 'random' -e 'Un' -e 'alt' -e 'EBV' > thous_genomes_GRCh38_full_analysis_set_plus_decoy_hla.fa.fai)  
   
   fi

# if not downloaded and filtered already, then download and filter dbSNP vcf resources
download_dbsnp() {
    local C=$1
    local dbSNP_bcf_dir=$2
    local n_threads=$3
    if [ ! -f $dbSNP_bcf_dir/00-All.vcf.gz ]; then
   
      wget https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh38p7/VCF/00-All.vcf.gz 
      wget https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh38p7/VCF/00-All.vcf.gz.tbi
   
   fi

    # Check if the file for the chromosome exists
    if [ ! -f "$dbSNP_bcf_dir/chrom_$C/dbsnp_v151.bcf" -a \
       "$C" != "X" ] || \
       [ "$C" = "X" -a ! -f "$dbSNP_bcf_dir/chrom_23/dbsnp_v151.bcf" -a \
       ! -f "$dbSNP_bcf_dir/chrom_$C/dbsnp_v151.bcf" ]; then
         
        # Download dbSNP data for the chromosome
        echo " " 
        echo "Downloading dbSNP for chromosome $C"
        (cd $dbSNP_bcf_dir && \
        bcftools view -i 'INFO/KGPhase3=1' -v snps --threads "$n_threads" \
                     -Ob -o "$dbSNP_bcf_dir/chrom_$C/dbsnp_v151.bcf" \
                     00-All.vcf.gz "$C"
        )
    fi
    
}


# Function to rename files and directories if "X" is present
rename_files_and_dirs() {

    local dbSNP_bcf_dir=$1
    
    # Rename files and directories containing "X" to "23"
    for item in "$dbSNP_bcf_dir"/*"X"*; do
        if [ -d "$item" ]; then
            # If it's a directory, rename it
            mv "$item" "${item/X/23}"
        fi
    done
}

# Loop through chromosomes 1 to 22 and X
for C in {1..22} X; do
    download_dbsnp "$C" "$dbSNP_bcf_dir" "$n_threads"
done

# Rename X files and directories to 23
rename_files_and_dirs "$dbSNP_bcf_dir"


# if [ $(find $dbSNP_bcf_dir/**/dbsnp_v151.bcf | wc -l) -ne 22 ]; then
# 
# if [ ! -f $dbSNP_bcf_dir/00-All.vcf.gz ]; then
# 
# wget https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh38p7/VCF/00-All.vcf.gz 
# wget https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh38p7/VCF/00-All.vcf.gz.tbi
# 
# fi
# 
# (cd $dbSNP_bcf_dir && \
#   seq 1 22 | while read C && [ ! -f $dbSNP_bcf_dir/chrom_${C}/dbsnp_v151.bcf ];
#   do bcftools view -i 'INFO/KGPhase3=1' -v snps --threads $n_threads\
#   -Ob -o chrom_${C}/dbsnp_v151.bcf \
#   00-All.vcf.gz "${C}" ; 
#   done;
# )
# 
# fi
    
# Use tabix to download relevant regions of 10000 genomes vcf
function download_thous_vcf { 

  #argument 1 is chromosome (INT, 1 to 22)
  #argument 2 is threads (INT, for bcftools compression / file writing)
  # If the chromosome number is 23 (i.e. X chrosome) then set chr (for downloading the file only)
  # as X
  if [ "$1" -eq 23 ]; then
     chr="X"
  else
     chr="$1"
  fi

  local cis_regions=output/gene_cis_regions/chrom_$1/cis_region_positions.txt

  # if the cis regions file for this chromsome is not empty (i.e. there are no cis regions to search)
  # then find and filter 1000 genomes vcfs ... (else run nothing)
  # this step is important - because if this file is empty, tabix tries to download the vcf for the entire
  # chromosome ... :(

  if [ -s $cis_regions ]; then
  
    echo "Getting relevant vcf info from 1000 genomes for chrom $chr"

    # define thousand genome file names and locations

    # temp (temporary location for more storage)
    local cis_regions_from_thous_temp=$(realpath -m --relative-to=$thous_genomes_temp_dir/chrom_$1 $cis_regions)

    # Step 1: Download relevant parts / regions of this chromosomes 1000 Genomes bcf
    # ftps links taken from here: https://www.internationalgenome.org/data-portal/data-collection/grch38
    # Temporarily change working directory so tabix index files are stored in the relevant 1000 genomes folder
    # And also - use bcftools to compress, and save as bcf
    
    
    (cd $thous_genomes_temp_dir/chrom_$1 && \
     tabix -h \
    ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20190312_biallelic_SNV_and_INDEL/ALL.chr$chr.shapeit2_integrated_snvindels_v2a_27022019.GRCh38.phased.vcf.gz \
    --regions $cis_regions_from_thous_temp |\
    bcftools view --threads $2 -Ob -o 1000genomes_GRCh38.bcf)

    fi
}


# Use bcftools to filter vcfs to only include biallelic SNPs, and that pass eQTL catalogue quality thresholds
function filter_thous_vcf {

    #argument 1 is chromosome (INT)
    #argument 2 is threads (INT, for bcftools compression / file writing)
    if [ "$1" -eq 23 ]; then
       chr="X"
    else
       chr="$1"
    fi


    # temp (temporary location for more storage)

    local thous_genomes_bcf=$thous_genomes_temp_dir/chrom_$1/1000genomes_GRCh38.bcf
    local thous_genomes_tagged=$thous_genomes_temp_dir/chrom_$1/1000genomes_GRCh38_tagged.bcf
    local thous_genomes_filt_bcf=$thous_genomes_temp_dir/chrom_$1/1000genomes_GRCh38_filt.bcf

    if [ -s $thous_genomes_bcf ]; then

    echo "Filtering vcf info from 1000 genomes for chrom $chr"

    # Step 2: Filter this bcf by quality  (missingness, hwe ...)
    # and by MAF, and only include biallelic SNPs
    # Filter steps adapted from the eqtl catalogue:
    # https://github.com/eQTL-Catalogue/genimpute/blob/master/modules/preimpute_QC.nf
    
    # got to calculate HWE per pop ... - otherwise filtering too much out 
    bcftools +fill-tags  $thous_genomes_bcf  -Ob -o $thous_genomes_tagged --threads $2 \
    -- -S output/thous_genomes_bcfs/sample_superpop_assignments_filt.txt -t HWE,F_MISSING 
    
    bcftools filter $thous_genomes_tagged -i 'F_MISSING < 0.05' -Ou |\
    bcftools filter -i 'EUR_AF > 0.01 | EAS_AF > 0.01 | AMR_AF > 0.01 | SAS_AF > 0.01 | AFR_AF > 0.01 ' -Ou |\
    bcftools filter -i 'HWE_EUR > 1e-6 | HWE_EAS > 1e-6 | HWE_AMR > 1e-6 | HWE_SAS > 1e-6 | HWE_AFR > 1e-6 ' -Ou | \
    bcftools filter -e 'REF="N" | REF="I" | REF="D"' -Ou |\
    bcftools filter -e "ALT='.'" -Ou |\
    bcftools view -m2 -M2 -v snps --threads $2 -Ob -o $thous_genomes_filt_bcf
    
    rm $thous_genomes_tagged

    # first line of above code chunk - add in extra required information / calculation
    # second-fouthline line - filter things based on hardy Weinberg, and maf, and missingness thresholds
    # third / forth line - filter out out cases where ref / alt information is lacking
    # fifth line - biallec snps only, output as compressed file

    fi

}


# Use dbSNP vcfs to add v151 rsIDs the 1000 genomes vcfs
function annotate_rsid_vcf {

    if [ "$1" -eq 23 ]; then
       chr="X"
    else
       chr="$1"
    fi

    #argument 1 is chromosome
    # argument 2 is threads (for bcftools compression / file writing)

    local thous_genomes_filt_bcf=$thous_genomes_temp_dir/chrom_$1/1000genomes_GRCh38_filt.bcf

    local cis_regions=output/gene_cis_regions/chrom_$1/cis_region_positions.txt

    if [ -s $thous_genomes_filt_vcf ] && [ -s $cis_regions ]; then

       echo "Annotating vcf info from 1000 genomes for chrom $chr"

       # Step 4:
       # Download relevant parts / regions of this chromosomes dbSNP vcf
       # (used to add v151 rsIDs to the 1000 genomes vcf)

       local dbSNP_bcf=$dbSNP_bcf_dir/chrom_$1/dbsnp_v151.bcf

       #local cis_regions_from_dbSNP_temp=$(realpath -m --relative-to=$dbSNP_temp_dir $cis_regions)

       # (cd $dbSNP_temp_dir && \
       #  tabix -h https://ftp.ncbi.nih.gov/snp/organisms/human_9606_b151_GRCh38p7/VCF/00-All.vcf.gz \
       #  --regions $cis_regions_from_dbSNP_temp |\
       #  bcftools view -v snps --threads $2 -Ob  -o $dbSNP_bcf && \
       #  bcftools index -f $dbSNP_bcf)

        # Step 5: annotate rsids with dbSNP
        bcftools index -f $thous_genomes_filt_bcf && bcftools index -f $dbSNP_bcf
        
        local thous_genomes_anno=$thous_genomes_temp_dir/chrom_$1/1000genomes_GRCh38_filt_dbSNPv151.bcf
                          
        # Add rsID where applicable 
        # Otherwise give variant IDs in the form, e.g. "1:1000" (chromosome_number:position)
        bcftools annotate -c CHROM,POS,ID \
                          -a $dbSNP_bcf \
                          $thous_genomes_filt_bcf  \
                          -R $cis_regions \
                          --threads $2 -Ou |\
        bcftools annotate --set-id +'%CHROM\:%POS\' -Ob -o  $thous_genomes_anno
                          
        #bcftools index -f $thous_genomes_anno
        
        rm $thous_genomes_filt_bcf
                          
    fi

}

# Put these downloading and filtering steps together
function prepare_thous_vcf {

  download_thous_vcf $1 $2 && \
  filter_thous_vcf $1 $2 && \
  annotate_rsid_vcf $1 $2

}

export -f download_thous_vcf
export -f filter_thous_vcf
export -f annotate_rsid_vcf
export -f prepare_thous_vcf

# Parralelise over chromosomes, such that chromosomes with a similar number of cis regions
# are done in sequence / at the same time (this should mean chromosomes which will take a similar amount of time
# are done at roughly the same time)

if [ -s output/gene_cis_regions/n_cis_regions.txt ]; then

   rm output/gene_cis_regions/n_cis_regions.txt
   
fi

for chrom_num in {1..23}
    do
    paste <(echo $chrom_num) \
          <(wc -l < output/gene_cis_regions/chrom_$chrom_num/cis_region_positions.txt) |\
          column -s $'\t' -t >> output/gene_cis_regions/n_cis_regions.txt 
    done


# also do things that are going to take longest first, so less likely of hanging of resources toward the end
chr_order=$(sort -r --version-sort  -k 2 output/gene_cis_regions/n_cis_regions.txt | awk '{print $1}')
chr_order=23
# for every chromosome (in parallel), extract and filter relevant information ...
parallel --jobs $n_jobs prepare_thous_vcf ::: $chr_order ::: $n_threads


module list

# do not - and I mean this Isobel - try to concatenate anything together it's a waste of your short life, and refuses to work
# please no more

```



