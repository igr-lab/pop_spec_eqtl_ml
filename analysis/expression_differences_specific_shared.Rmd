---
title: "Expression differences between specific and shared eQTLs"
output:
  workflowr::wflow_html:
    toc: true
    code_folding: show
editor_options:
  chunk_output_type: console
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.width = 20, fig.height = 10)
```

# Set up 

```{r setup_2, message = FALSE, warning=FALSE,collapse=TRUE, class.source = 'fold-hide'}

library(ggplot2)
library(dplyr)
library(data.table)
library(ggpubr)
library(reactable)
library(patchwork)

# Set up custom theme for ggplots
custom_theme <-
  list(
    theme_bw() +
      theme(
        panel.border = element_blank(),
        axis.line = element_line(),
        text = element_text(size = 9),
        legend.position = "bottom",
        strip.background = element_blank(),
        axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)
      ) 
  )

```

## Loading Indonesian eQTL summary statistics (prior to applying mashr)

```{r  setup_3, message = FALSE, warning=FALSE, class.source = 'fold-hide'}
# eQTLs summary statitics prior to mashr analysis 
natri_indo = data.table::fread("data/eqtl_summary_stats/Natri_2020/Natri_2020_Indo_blood.tsv")

# add column names
names(natri_indo) = c("target",
                      "target_chromosome", 
                      "target_start",
                      "n_tested_snps",
                      "snp_distance_to_target",
                      "rsid",
                      "snp_position",
                      "nominal_p",
                      "slope")

# combine gene and snp columns 
natri_indo = natri_indo %>% 
              tidyr::unite("gene_snp", c("target", "rsid"))

```

## Loading Indonesian specific called eQTLs

```{r setup_4, message=FALSE, warning=FALSE, class.source = 'fold-hide'}

# eQTLs significant in indonesian pop.  lsfr < 0.05
# specific if lsfr > 0.1 in secondary populations
# from usng mashr - 
indo_spec = read.table("output/ml_data/natri_indo_eqtls_lsfr0.05.txt",
                       sep = "\t")

indo_spec = indo_spec %>% 
            tibble::rownames_to_column("gene_snp")

# add binary column of population specific and shared 
indo_spec = indo_spec %>% 
            rowwise() %>% 
            mutate(class_binary = ifelse(Class == "Shared", 0, 1)) %>% 
            ungroup()

```

## Creating permutation test function (for stats testing of the differences of means)

```{r setup_5, message=FALSE, warning=FALSE, class.source = 'fold-hide'}

# permutation test of means

permutation_mean_test <- function(feature, # feature vector
                             class, #class binary column vector
                             n){ # number of permutations to perform
  
  
# Adapting permutation test code from
# https://thomasleeper.com/Rcourse/Tutorials/permutationtests.html
# and https://towardsdatascience.com/permutation-test-in-r-77d551a9f891  
  
# Step 1: Calculate difference in means (this is the test statistics in the difference in means)  

test_stat =   diff(
                   by(feature,
                   class, 
                   function(x) mean(x, na.rm = T)
                                                  )
                                                    )

require(parallel)

cl = parallel::makeCluster(future::availableCores())

parallel::clusterSetRNGStream(cl)

# Step 2: Calculate difference in means for n permutations
# This is the distribution of the test statistics  

# Do this in parallel: 

dist_stats = parallel::parSapply(cl, 1:n, function(i,...) { diff(
                                          by(feature ,
                                          sample(class, length(class)), 
                                                function(x) mean(x, na.rm = T)
                                                                               )) } )


parallel::stopCluster(cl)
                                                    

# Step 3: Perform a two tailed
# Compare the test statistic with the distribution of the test statistic

two_tailed_test = sum(abs(dist_stats) > abs(test_stat))/n

return(two_tailed_test)  

  
}  

```


# Original Distribution of Expression metrics

## GTEx

```{r within_tisse_metrics, class.source = 'fold-hide'}

indo_spec %>% 
  group_by(Class) %>% 
  summarise(across(contains("wb"), ~mean(.x, na.rm = T))) %>% 
  mutate(across(!Class, ~round(.x, digits = 2))) %>% 
  reactable()

(
  (indo_spec %>% 
  ggviolin(y="wbmed_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y = "log (1+ tpm)",
       title = "Median Expression") 
                                      )+ 
(
 indo_spec %>% 
  ggviolin(y="wbmean_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y = "log (1+ tpm)",
       title = "Mean Expression")  
                                    ) + 
(
 indo_spec %>% 
  ggviolin(y="wbmode_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y = "log (1+ tpm)",
       title = "Mode of Expression")  
)    
    
  ) /
(
  (
  indo_spec %>% 
  ggviolin(y="wbmax_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "log (1 + tpm)",
       title = "Maximum Expression") ) + 
(
 indo_spec %>% 
  ggviolin(y="wbmin_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "log (1 + tpm)",
       title = "Minimum Expression") 
                                     ) + 
(
 indo_spec %>% 
  ggviolin(y="wbvar_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y = "log (1+ tpm)",
       title = "Variance of Expression")  
)  
 )


```

# Is the higher average expression in Indonesians explained by post-hoc / pre-hoc divide? 

```{r pre_hoc_eqtls, class.source = 'fold-hide'}

# top 1,975 eqtls in natri et al were originally called in the indonesian dataset
# select these: 

prehoc_indo = natri_indo %>% 
              slice_min(nominal_p, n = 1975)


indo_spec_hoc= indo_spec %>% 
                mutate(when_called = ifelse(gene_snp %in% prehoc_indo$gene_snp, 
                                            "pre-hoc", 
                                            "post-hoc")) %>% 
                tidyr::unite("Class", c("Class", "when_called"), sep = ",") 


print("Number of pre and post hoc eQTLs by shared or specific classes")
indo_spec_hoc %>% 
  group_by(Class) %>% 
  summarise(n = n()) %>% 
  reactable()

print("Mean value of expression metrics across pre and post hoc eQTLs")
indo_spec_hoc %>% 
  group_by(Class) %>% 
  summarise(across(contains("wb"), ~mean(.x, na.rm = T))) %>% 
  mutate(across(!Class, ~round(.x, digits = 2))) %>% 
  reactable()

(
  (indo_spec_hoc %>% 
  ggviolin(y="wbmed_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y = "log (1+ tpm)",
       title = "Median Expression") 
                                      )+ 
(
 indo_spec_hoc %>% 
  ggviolin(y="wbmean_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y = "log (1+ tpm)",
       title = "Mean Expression")  
                                    ) + 
(
 indo_spec_hoc %>% 
  ggviolin(y="wbmode_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y = "log (1+ tpm)",
       title = "Mode of Expression")  
)    
    
  ) /
(
  (
  indo_spec_hoc %>% 
  ggviolin(y="wbmax_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "log (1 + tpm)",
       title = "Maximum Expression") ) + 
(
 indo_spec_hoc %>% 
  ggviolin(y="wbmin_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "log (1 + tpm)",
       title = "Minimum Expression") 
                                     ) + 
(
 indo_spec_hoc %>% 
  ggviolin(y="wbvar_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y = "log (1+ tpm)",
       title = "Variance of Expression")  
)  
 )


```

# Specifically compare post / pre-hoc divide with minimum expression 

```{r compare, fig.height=8, fig.width=8}

 indo_spec_hoc %>% 
  ggviolin(y="wbmin_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "log (1 + tpm)",
       title = "Minimum Expression")  + 
   indo_spec %>% 
  ggviolin(y="wbmin_log_1p_tpm", 
           x = "Class", 
           fill = "Class",
           add = "boxplot") + 
  labs(y= "log (1 + tpm)",
       title = "Minimum Expression") 

```

# Distribution of p-values for specific and shared eQTls?

```{r significance_pre_mashr, fig.height=8, fig.width = 10}

sig_pre_mashr = inner_join(indo_spec, 
                           natri_indo %>% select(gene_snp,
                                                 nominal_p))
                

sig_pre_mashr %>% 
  ggplot(aes(x=nominal_p)) + 
  geom_histogram() + 
  custom_theme + 
  facet_wrap(~Class)


```

# Breaking by measures of significance

```{r sig_breaks}

print("Quartiles of significance")

sig_pre_mashr  %>% 
  filter(Class == "indo-specific") %$%
  nominal_p %>% 
  summary()

```

