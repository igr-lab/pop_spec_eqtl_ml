---
title: "Figure 3 (Part 2 - Power Associated Factors)"
date: "2023-10-09"
output: workflowr::wflow_html
editor_options:
  chunk_output_type: console
---

## Set up

### Libraries + variant information files:

```{r setup, echo = FALSE}

library(dplyr)
library(dtplyr)
library(data.table)
library(circlize);
library(viridis);
suppressMessages(library(ComplexHeatmap))


# use feature extraction functions from ml pipeline
# for getting LDScores
options(box.path = here::here())
box::use(code/feature_extraction)

# load git committing utility functions 
box::use(code/utils/workflowr_git_utils[...])
box::use(code/utils/paper_plotting_functions[...])

# if changed these utility functions, commit them 
wflow_dpen_commit(path = list("code/utils/workflowr_git_utils.R",
                              "code/feature_extraction"),
                  message = "Update getting features pipeline for figure 3 of ra paper")


hg19_var_info = data.table::fread("data/eqtl_summary_stats/whole_blood/Indo_Natri_2022/indonesia_varinfo.tsv",
                                  select = c("rsid", "pos")) %>% 
                dplyr::rename(hg19_position = pos)

gnomad_ld_df <- data.table::fread("output/gnomad_ld_scores/ld_score_cross_pop.csv") 

```

### What datasets and features to plot per dataset / population?

```{r plot_which_data_fns}

dataset_to_pops = function(dataset){

  if(dataset == "mak"){

    return( c("aa", "pr", "mx"))

  } else if(dataset == "mogil") {


    return(c("aa", "ea", "his"))

  } else {

    return(c("indo", "lepik",  "gtex", "twinsuk"))
  }

}


maf_pops_ls = list(mak = list(aa = "aa",
                        pr = "pr",
                        mx = "mx"),
                natri = list(indo = "indo",
                          lepik = "lepik",
                          twinsuk = "twinsuk",
                          gtex = "gtex"),
                mogil = list(aa = "AF_afr",
                             his = "AF_amr",
                             ea = "AF_nfe_nwe"
                )
                )


    all_gnomad_pops = c("African_American",
                        "Ashkenazi_Jewish",
                                    "Latino_Admixed_American",
                                    "North_Western_European",
                                    "East_Asian",
                                    "Southern_European",
                                    "Estonian")

gnomad_maf_pops_ls = list(mogil =          
                             list(aa = "African_American",
                                     his = "Latino_Admixed_American",
                                     ea = "North_Western_European",
                                     oth = "East_Asian"
                                     ),
                       mak =  list(aa = "African_American",
                                     pr = "Latino_Admixed_American",
                                     mx = "Latino_Admixed_American",
                                     oth =  c("North_Western_European", "East_Asian")
                                     ),
                       natri =  list(gtex = "North_Western_European",
                                        lepik = "North_Western_European",
                                        twinsuk = "North_Western_European",
                                        indo = "East_Asian",
                                        oth =  c("African_American", "Latino_Admixed_American")
                       )
)




maf_to_plot = function(dataset, 
                       port_from, 
                       port_to){
  
  maf_plot_vec = 
  c(
    maf_pops[[dataset]][[port_from]],
    maf_pops[[dataset]][[port_to]]
    )
  
}

ld_score_to_plot = function(dataset,
                            port_from,
                            port_to) {
  
  
  if(
      (port_from == "aa" & port_to == "his")| 
      (port_from == "his" & port_to == "aa")
      ){
    
    ld_score_list = c("African_American_LDScore",
                      "Latino_Admixed_American_LDScore",
                      "North_Western_European_LDScore",
                      "East_Asian_LDScore")
    
  } else if (
             (port_from == "ea" & port_to == "aa") |
             (port_from == "aa" & port_to == "ea")
             ) {
    
        ld_score_list = c("African_American_LDScore",
                          "Latino_Admixed_American_LDScore",
                          "North_Western_European_LDScore",
                          "East_Asian_LDScore")
  
  } else {
  
          ld_score_list = c("African_American_LDScore",
                          "Latino_Admixed_American_LDScore",
                          "North_Western_European_LDScore",
                          "East_Asian_LDScore")
}

return(ld_score_list)

}


```

### Getting data functions

```{r getting_data_fns}

# from filename (character) of either effect size or nominal pvalue portability results
# (i.e. the .tsv files produced during analysis from Fig_2_upset_plots) 
# load this file, and keep the portability metrics (either effect size ratio or nominal pvalue), 
# and then format it so that it is clear what portability direction it refers

get_ratios_or_sigs = function(filename, dataset){
  
  df = data.table::fread(paste0("paper_figures/fig_2/", filename))
  
  
  # get the name of the porting from population from the filename
  port_from = stringr::str_extract(filename,
                                             "(?<=\\.)[a-z]+(?=\\.to)")
  
  port_to = dataset_to_pops(dataset)[dataset_to_pops(dataset) != port_from]
  
  # select only portability metric columns, and ID (gene_snp) columns
  # format appropriately
  ratio_df = df %>% 
  select(gene_snp, any_of(c(
                            paste0(port_to, "_ratio"),
                            paste0(port_to, "_nom_pval"))
                          )
         ) %>% 
  rename_with(.fn = ~paste0(port_from,"_", .x), 
              .cols = any_of(c(contains("nom_pval"), 
                               contains("ratio")
                               )
                             )
              )
  
  return(ratio_df)
}


# from a set name (character, one of "mak", "natri", "mogil")
# load all the portability metrics for all porting directions / pairs
get_port_metrics = function(set_name){
  
 # what files do we need to load for this set of integrated eqtl sum stats
  
set_files  = c(list.files("paper_figures/fig_2", paste0(set_name,
                                                        ".*.",
                                                        "effect_size_ratio_0.5.tsv"),
                      ),
               list.files("paper_figures/fig_2/", paste0(set_name,
                                                        ".*.",
                                                        "nom_pvalue_thresh_0.05.tsv")
                     )
                )

set_files = grep(".rds", 
                 set_files,
                 invert = T, 
                 value = T)
 
 dataset_ratios = purrr::map(set_files, 
                             ~get_ratios_or_sigs(.x, set_name)) %>% 
                      Reduce("full_join", .)
 
 return(dataset_ratios)

}



# convert (using rsIDs, and info from Indonesian variant information) hg38 positions to hg19
rs_hg38_to_hg19 = function(annotated_df, 
                           hg19_var_info) {
  
#  browser()
  
 rsid_df = annotated_df %>% 
           dplyr::mutate(hg38_position = position) %>% 
           dplyr::rename(rsid = snp) %>% 
           dplyr::select(chromosome, rsid, hg38_position, ref, alt) %>% 
           dplyr::distinct() %>% 
           as.data.frame()
  
hg19_from_indonesia_df = left_join(rsid_df,
                                    hg19_var_info, 
                                   by = "rsid")


n_rsids_left = hg19_from_indonesia_df %>%
                  filter(is.na(hg19_position)) %>% 
             nrow()

if(n_rsids_left >= 1){
 
hg19_gr_to_test = hg19_from_indonesia_df %>%
                  filter(is.na(hg19_position)) %>% 
                  mutate(seqnames = paste0("chr", chromosome),
                                   start = hg38_position,
                                   width = 1) %>% 
                           plyranges::as_granges()
                           
                            
 library(rtracklayer)

 chain <- import.chain("segment_liftover/hg38ToHg19.over.chain")
 
seqlevelsStyle(hg19_gr_to_test) = "UCSC"
cur19 = liftOver(hg19_gr_to_test, chain)
hg19_gr_liftovered = unlist(cur19) %>% 
                     as.data.frame() %>% 
                      select(rsid, 
                             hg19_position)


hg19_rsid_pos = rows_update(hg19_from_indonesia_df, hg19_gr_liftovered, by = "rsid") 

} else {
  
  hg19_rsid_pos = hg19_from_indonesia_df
}

# Check how much is lost by doing this: 
n_dropped_rsids = nrow(hg19_rsid_pos) - nrow(rsid_df)

message(n_dropped_rsids, " of ", nrow(rsid_df), " could not be mapped to hg19 for LDScore mapping ...")

return(hg19_rsid_pos)
}



# from a feature_df (a dataframe with at least: a column refering to chromosome,
# position (i.e. hg38 snp position), and rsid (column name snp)
# get the LDScore metrics from gnomAD 
get_ldscore_info = function(annotated_df){
  
  hg19_rsid_pos = rs_hg38_to_hg19(annotated_df, hg19_var_info)
  
  hg19_rsid_pos = hg19_rsid_pos %>% 
                  dplyr::mutate(start = hg19_position) 
  
  
  ldscore_df = left_join(hg19_rsid_pos, 
                         gnomad_ld_df,
                         by = c("chromosome", "start", "ref", "alt"))
  
  ldscore_add_df = left_join(annotated_df, 
                             ldscore_df %>% dplyr::select(snp = rsid,
                                                   contains("LDScore")))
  # 
  # ldscore_df = feature_extraction$extract_from_rsid(gr = plyranges::as_granges(hg19_rsid_pos_filt),
  #                                                   snp_features_df = hg19_rsid_pos_filt,
  #                                                  features = "ld_score_cross_pop_measures") %>% 
  #               select(snp = refsnp_id, 
  #                      hg19_position = start,
  #                      contains("LDScore"))
  
  #ldscore_add_df = left_join(annotated_df, lazy_dt(ldscore_df), by = "snp")
  
  
  return(ldscore_add_df)
  
}


add_dataset_features = function(set_name) {
  
  # Firstly let's load and format all the portability metric data for this set of summary stats
  set_portability = suppressMessages(get_port_metrics(set_name)) %>% 
                    tidyr::separate(gene_snp, 
                                    into = c("gene", "snp"), 
                                    sep = "_")
  
  
  ##### first annotation step  ###### 
  # the effect size and maf - get from combined eqtl summ stats
  
  # Now let's load the combined eqtl summary statistics information for this set: 
  sum_stats_file = paste0("output/combined_eqtl_summary_stats/combined_", 
                          set_name, ".csv"
                          )

  full_column_names = names(data.table::fread(sum_stats_file, 
                                              nrows = 0)
                            )
                            
  columns_to_get =  c("gene",
                      "snp",
                      "position",
                      "chromosome",
                      "ref",
                      "alt",
                      full_column_names[grepl("maf|beta|AF",
                                              full_column_names
                            )]
                      )
  
  
  combined_df = lazy_dt(data.table::fread(sum_stats_file, 
                                          select = columns_to_get)
                        )
  
  annotated_df = left_join(lazy_dt(set_portability), 
                           combined_df, 
                           by = c("gene", "snp")
                           ) %>% 
                  mutate(chromosome = as.character(chromosome)
                         ) %>% 
                 as.data.frame()
  
  ######  second annotated step; get ldscore information from gnomAD #####

  
  annotated_df = get_ldscore_info(annotated_df)
  
  annotated_df = annotated_df %>% 
                 dplyr::select(-position, -chromosome, -ref, -alt) %>% 
                 tidyr::unite("gene_snp", gene:snp)# 
  
  
  annotated_df = annotated_df %>% 
                 group_by(gene_snp)
  
  browser()
  
  annotated_v1 = annotated_df %>% 
                 slice_head(n=1)
  annotated_v2 = annotated_df %>% 
                 slice_tail(n=1)
  
  annotated_df = rows_patch(annotated_v1, 
                            annotated_v2) %>% 
                distinct()
  # %>% 
  #                distinct() %>% 
  #                filter(if_all(
  #                                     all_of(c(
  #                                              contains("ratio"),
  #                                              contains("pval"))
  #                                            ),
  #                              ~.x!=0|!is.na(.x)
  #                                     )
  #                       ) %>% 
  
  annotated_df = annotated_df %>% 
                 as.data.frame() %>% 
                 tibble::column_to_rownames("gene_snp")
  
  return(annotated_df)
  
}

```

### Make correlation dataset functions (no longer run)
 
````{r format_matrix_fns, eval = FALSE}

format_df_plot = function(annotated_df,
                       dataset,
                       maf_pops = base::unique(Reduce(c, maf_pops_ls[[dataset]])),
                       gnomad_pops = base::unique(Reduce(c,gnomad_maf_pops_ls[[dataset]]))
                    ){
                         
  
  ######### MAF ###############
  
  # which cols to keep or remove ...
 
  all_maf_cols = grep("AF|maf", 
                      names(annotated_df), 
                      value = TRUE
                      )
  
  maf_cols_to_keep = grep(paste0(c(gnomad_pops,
                                   maf_pops), 
                                 collapse = "|"), 
                          all_maf_cols, 
                          value = TRUE)
  
  maf_to_remove = setdiff(all_maf_cols,
                          maf_cols_to_keep)
  
  annotated_df = annotated_df %>% 
                 dplyr::select(-all_of(maf_to_remove)
                               )
  
  #### now let's calculate pairwise difference across all datasets in this set
  
  maf_pairs_list = combn(maf_cols_to_keep, 
                         2, 
                         simplify = FALSE) 
  
  names(maf_pairs_list) = purrr::map_chr(maf_pairs_list,  
                                         ~stringr::str_c(.x,collapse = "-"))
  
  maf_pairs_df = maf_pairs_list %>% 
    purrr::map_dfc(., 
                      function(pairs) {
                                       pull(annotated_df, pairs[1]) - 
                                       pull(annotated_df, pairs[2])
                                       }
                    
                  )
  
  annotated_df = cbind(annotated_df, 
                       maf_pairs_df)

  
  ########## LD ######################
  
  # secondly, let's do the same thing for LDScores
  # removing not-important ld data
 
   ldscore_cols = grep("_LDScore", 
                       names(annotated_df),
                       value = TRUE
                       )
   
   # step i. remove anything that is LD - another non-LD metric
 
   ldscore_cols_to_keep = grep(paste0(c("beta",
                                     "AF",
                                     "pval",
                                     "ratio",
                                      "var", 
                                      "mean"), collapse = "|"),
                            ldscore_cols,
                            invert = TRUE,
                            value  = TRUE)
   
   # step. ii. remove anything that is LD for a non-requested for pop
   
   ldscore_cols_to_keep = grep(paste0(gnomad_pops, 
                                       collapse = "|"),
                                ldscore_cols_to_keep,
                               value = TRUE
                                )
   
  
    # now remove all these unwanted columns
   
   ld_to_remove = setdiff(ldscore_cols, 
                          ldscore_cols_to_keep)
   
   annotated_df = annotated_df %>% 
                  dplyr::select(-all_of(ld_to_remove)
                                )
  
 #  ldscore_cols = grep("_LDScore", 
 #                       names(annotated_df),
 #                       value = TRUE
 #                       )
 #  
 # 
 # 
 #   names(ldscore_pairs_list) = 
   
    # calculate LDscore differences across requested pairs
   
    ldscore_pairs_list = combn(paste0(gnomad_pops, "_LDScore"
                                    ), 
                             2, 
                             simplify = FALSE) 
    
    ldscore_pairs = ldscore_pairs_list %>% 
                    purrr::map_chr(.,  
                                   ~stringr::str_c(.x,collapse = "-"))
    
    names(ldscore_pairs_list) = ldscore_pairs
       
  ldscore_pairs_df = ldscore_pairs_list %>%
    purrr::map_dfc(.,
                      function(pairs) {
                                       pull(annotated_df, pairs[1]) - 
                                       pull(annotated_df, pairs[2])
                                       }

                  )
  
 #  
 annotated_df = cbind(annotated_df, 
                      ldscore_pairs_df)
 
 return(annotated_df)
 
}


make_cor_df = function(annotated_df,
                       dataset,
                       maf_pops = base::unique(Reduce(c, maf_pops_ls[[dataset]])),
                       gnomad_pops = base::unique(Reduce(c,gnomad_maf_pops_ls[[dataset]]))
                    ){
 # 
  
  annotated_df = format_df_plot(annotated_df,
                       dataset,
                       maf_pops,
                       gnomad_pops)
  
 # Now let's calculate the correlation between p-value based portability metric, and effect size one
 # to everything else (LDScore, effect size, maf)
  cor_df = annotated_df %>% 
    corrr::correlate(
                     method = "spearman",
                     use = "pairwise.complete.obs",
                     quiet = TRUE) %>% 
    corrr::focus(contains("_nom_pval_"), contains("ratio")) 
  
  return(cor_df)
}


```

### Functions for plotting correlations (no longer run)

````{r plot_cor_fns, eval = FALSE}

col_fun = colorRamp2(seq(from = -1, to = 1.00, by = 0.2),
                     rev(RColorBrewer::brewer.pal(n = 11, "RdBu")),
                     space = "RGB")


plot_cor = function(port_from, 
                    port_to,
                    dataset, 
                    cor_df,
                    maf_pops = maf_pops_ls[[dataset]],
                       gnomad_pops = gnomad_maf_pops_ls[[dataset]]
                    ) {
  
  cor_df = cor_df %>% 
            dplyr::select(contains(paste0(port_from,
                                   "_",
                                   port_to)
                            ),
                          contains(paste0(port_from,
                                   "_nom_pval_",
                                   port_to)),
                         term)
  
  all_maf_terms = grep("AF|maf",
                    cor_df$term, 
                    value = TRUE) 
  
 maf_only_terms_to_keep = grep(pattern = "-",
                       all_maf_terms ,
                       value = TRUE,
                       invert = TRUE
                       ) %>% 
                   grep(pattern = paste0(c(maf_pops[[port_from]],
                                         maf_pops[[port_to]],
                                         gnomad_pops[[port_from]],
                                         gnomad_pops[[port_to]]),
                                         collapse = "|"
                                         ),
                        .,
                        value = TRUE
                        )
 
 maf_pairs_to_keep = grep(pattern = "-",
                          all_maf_terms ,
                          value = TRUE
                          ) %>% 
                    grep(pattern = paste0(c(maf_pops[[port_to]],
                                            gnomad_pops[[port_to]]
                                            ),
                                         collapse = "|"
                                         ),
                         .,
                         value = TRUE
                    )
  
  maf_terms_to_remove = setdiff(all_maf_terms, c(maf_pairs_to_keep, 
                                                 maf_only_terms_to_keep)
                                )
  
  # for ld
  
  all_ld_terms = grep("LD",
                    cor_df$term, 
                    value = TRUE) 
  
  
   ld_only_terms_to_keep = grep(pattern = "-",
                       all_ld_terms ,
                       value = TRUE,
                       invert = TRUE
                       ) %>% 
                   grep(pattern = paste0(c(
                                          gnomad_pops[[port_from]],
                                         gnomad_pops[[port_to]]
                                         ),
                                         collapse = "|"
                                         ),
                        .,
                        value = TRUE
                        )
   
   
   ld_pairs_to_keep = grep(pattern = "-",
                          all_ld_terms ,
                          value = TRUE
                          ) %>% 
                    grep(pattern = gnomad_pops[[port_to]],
                         .,
                         value = TRUE
                    )
   
    ld_terms_to_remove = setdiff(all_ld_terms, c(ld_pairs_to_keep, 
                                                 ld_only_terms_to_keep)
                                )
    
  
 
    
    
    
    
    cor_df = cor_df %>% 
             filter(!(term %in% ld_terms_to_remove)) %>% 
             filter(!(term %in% maf_terms_to_remove))
    
    
    
  cor_df = cor_df %>% 
           #pivot_wider()
           rename_with(~"Significance", contains("nom_pval")) %>% 
           rename_with(~"Effect Size", contains("ratio"))
  
  
  cor_mat = cor_df  %>% 
          tibble::column_to_rownames("term") %>% 
          as.matrix()
  
     
    #### best bio-signal/highest relevance terms 
    
    best_bio = c(
      # LDscore difference between porting from and porting to pop
                  grep(pattern = gnomad_pops[[port_from]], 
                              ld_pairs_to_keep,
                              value = TRUE
                    ),
      # MAF difference between porting from and to 
               grep(pattern = paste0(c(maf_pops[[port_from]],
                                       gnomad_pops[[port_from]]
                                       ), 
                                     collapse = "|"
                                     ),
                    maf_pairs_to_keep,
                    value = TRUE
                    )
           
    )
    
    mixed_bio = c( 
      # firstly ... MAF of porting to population:
                    grep(paste0(c(gnomad_pops[[port_to]],
                                maf_pops[[port_to]]),
                                collapse = "|"
                                ),
                       maf_only_terms_to_keep,
                       value = TRUE
                       ),
      # secondly ... LDscore of porting to pop
        grep(gnomad_pops[[port_to]],
                       ld_only_terms_to_keep,
                       value = TRUE
                       ),
      # and now, effect size of porting to pop
                  grep(paste0(maf_pops[[port_to]], "_beta",
                              "|", port_to, "_beta"),
                       rownames(cor_mat),
                       value = TRUE)
                  )
    
    # little less relevant
    less_bio = c(
      # Effect size of porting from population
                  grep(paste0(gnomad_pops[[port_from]], "_beta", 
                              "|", maf_pops[[port_from]], "_beta", 
                              "|", port_from, "_beta"),
                       rownames(cor_mat),
                       value = TRUE),
        # maf of porting from pop
                  grep(maf_pops[[port_from]],
                       maf_only_terms_to_keep,
                       value = TRUE
                 ),
                  grep(gnomad_pops[[port_from]],
                       ld_only_terms_to_keep,
                       value = TRUE
                 )
    )
    
    
    #### no bio-signal terms
    
    no_relevance_bio = setdiff(
                              rownames(cor_mat),
                               c(best_bio, 
                                 mixed_bio,
                                 less_bio)
                           )
      
  
  
  # cor_mat = cor_mat[grep(paste(port_from, 
  #                              port_to, 
  #                              "LDScore", 
  #                              sep = "|"), 
  #                        rownames(cor_mat)), ]
  
 
  
  maf = grep("maf|gnomAD_AF", rownames(cor_mat), value = T)
  beta = grep("beta", rownames(cor_mat), value = T)
  ld =  grep("LDScore", rownames(cor_mat), value = T)
  
  order_bio = function(bio_vec) {
    
   ord_bio_vec  = bio_vec[c(which(bio_vec %in% ld),
              which(bio_vec %in% maf),
              which(bio_vec %in% beta)
              )]
   
   return(ord_bio_vec)
   
  }
  
  
   bio_order = c(order_bio(best_bio), 
                 order_bio(mixed_bio), 
                 order_bio(less_bio), 
                 order_bio(no_relevance_bio)
                 )
  
  
  cor_mat = cor_mat[c(bio_order), ]
  
  
  type_anno = dplyr::case_when(grepl("maf|gnomAD_AF", rownames(cor_mat)) ~ "MAF",
                               grepl("beta", rownames(cor_mat)) ~ "Effect size",
                               grepl("LDScore", rownames(cor_mat)) ~ "gnomAD LDScore")
  
  
  bio_rel_anno = dplyr::case_when(rownames(cor_mat) %in% best_bio ~ "Highest",
                                  rownames(cor_mat) %in% mixed_bio ~ "Middle",
                                  rownames(cor_mat) %in% less_bio ~ "Middle-Low", 
                                  rownames(cor_mat) %in% no_relevance_bio ~ "Limited Relevance"
                                  )
  
  rownames(cor_mat) = rownames(cor_mat) %>% 
  gsub("beta", "Effect size", .) %>% 
  gsub("_", " ", .) %>% 
  stringr::str_to_title() 
  
    
rownames(cor_mat)  = sapply(rownames(cor_mat), 
                            function(n) {
    
    if(grepl("-", n)) {
      
       
      n = gsub(",", " ", n) 
      
      if(grepl("Ldscore", n)) {
        
      n = paste0("$\\Delta$ LDScore (", n, ")"
                            )
        
      } else {
      
        n = paste0("$\\Delta$ MAF (", n, ")"
                            )
        
      }
      
      
      
      
    }
    
    return(n)
  }
)
  
  
  rownames(cor_mat) = rownames(cor_mat) %>% 
  gsub("Maf ", "MAF", .) %>% 
  gsub("Ldscore ", "LDScore ", .) %>% 
  gsub("Ldscore", "LDScore", .) %>% 
  gsub("Gnomad Af ", "MAF (gnomAD)", .) %>% 
  gsub("\\bAmr\\b", "Latino Admixed American", .) %>% 
  gsub("\\bAfr\\b", "African American", .) %>% 
  gsub("Aa ", "African-American ", .) %>% 
  gsub("Nfe Nwe", "North Western European", .) %>%   
  gsub("Ea ", "European-American ", .) %>% 
  gsub("His ", "Hispanic ", .) %>% 
  gsub("Pr", "Puerto-Rican ",. ) %>% 
  gsub("Mx", "Mexican-American ", .) %>% 
  gsub("Gtex", "GTEx", .) %>% 
  gsub("Lepik", "Estonian Biobank", .) %>% 
  gsub("Twinsuk", "TwinsUK", .) %>%  
  gsub("Indo", "Indonesian", .)
  
 

 
  row_factors = rowAnnotation(Bio_Rel =  anno_simple( 
                                                      bio_rel_anno,
                                                       col =  c("Highest" = "#525252",
                                                                "Middle" = "#969696",
                                                                "Middle-Low" = "#969696", 
                                                               "Limited Relevance" = "white"),
                                                      width = unit(1, "mm")
                              ),
                              Features =  
                                         anno_simple( 
                                                     type_anno,
                                                     col =  c("MAF" = "#882255",
                                                              "Effect size" ="#117733",
                                                              "gnomAD LDScore" = "#E7D731"
                                                              ),
                                                     width = unit(1, "mm")
                                                     ),
                               annotation_legend_param = list(labels_gp = gpar(fontsize = 6,
                                                                              fontface = "plain",
                                                                              fontfamily="Helvetica"),
                                                           title_gp=gpar(fontsize=6,
                                                            fontface="plain",
                                                           fontfamily="Helvetica")
                                                           ),
                              show_annotation_name = FALSE,
                              annotation_name_gp = grid::gpar(fontsize = 6,fontfamily="Helvetica"),
                              #annotation_width = unit(0.3, "mm"),
                              #annotation_height = unit(0.1, "mm"),
                             # height = unit(1, "mm"),
                              #width = unit(0.1, "mm"),
                              gp =  grid::gpar(fontsize = 6,fontfamily="Helvetica"),
                              show_legend = c("Features" = FALSE,
                                              "Bio_Rel" = FALSE)
                              )
  
   pdf(file = paste0("paper_figures/fig_3/", 
                     port_from,
                     "_to_",
                     port_to,
                    "_power_corr_heatmap.pdf"),
          width=40*0.0393701,
          height=50*0.0393701)

 draw(ComplexHeatmap::Heatmap(mat = cor_mat, 
          col = col_fun,
          #na_col = "#FFFFFF", 
          # width = box_dim,
          # height = box_dim,
          width = unit(1.5, "cm"),
          #height = box_dim,
          column_names_rot = 45,
          column_names_side = "top",
          column_names_gp = grid::gpar(fontsize = 4,fontfamily="Helvetica"),
          column_names_centered = FALSE,
          row_names_gp = grid::gpar(fontsize = 2, fontfamily="Helvetica"),
          #row_names_rot = 10,
          row_order = rownames(cor_mat),
          show_row_dend = FALSE,
          #column_labels = c("FDR < 0.05", "Ratio < 0.05"),
          show_column_dend = FALSE,
          #show_column_names = FALSE,
          show_row_names = TRUE,
          row_names_max_width = unit(1, "cm"),
          #column_names_max_height = unit(1, "cm"),
          #clustering_distance_rows = function(m) dist(m),
          show_heatmap_legend  = FALSE,
          row_labels = latex2exp::TeX(rownames(cor_mat)),
          column_order = c("Significance", "Effect Size"),
          right_annotation = row_factors,
           cell_fun = function(j, i, x, y, width, height, fill) {
        grid.text(sprintf("%.2f", cor_mat[i, j]), x, y, gp = gpar(fontsize = 4))
                                                                 }
          
  )
 )
 
 dev.off()

  
  
}


```

## Get datasets for plotting

```{r get_dataset_features}

sets = c("natri", "mogil", "mak")
#sets = "mogil"

feature_df_list = purrr::map(sets,
                         function(dataset) {
                                             feature_df = add_dataset_features(dataset) #%>% 
                                                      #make_cor_df(., dataset)

                                             data.table::fwrite(feature_df, paste0("paper_figures/fig_3/", "dataset_features_", dataset, ".csv"))

                                              return(feature_df)

                                               }
                         )

names(feature_df_list) = sets

```

## Plot bby plot

### Plotting correlations (no longer run)

#### Plotting legend 

```{r plot_legend, eval = FALSE}

      pdf(file = paste0("paper_figures/fig_3/", 
                        "power_cor", 
                        "fig_legend.pdf"),
          width=15*0.0393701,
          height=40*0.0393701)



draw(Legend(col_fun = col_fun, 
       at = c(-1, -0.5, -0.25, 0, 0.25, 0.5, 1),
       title = "r^2",
       legend_height = unit(2.5, "cm"),
       gap = unit(1, "mm"),
       size = unit(1, "mm"),
       title_gap = unit(1, "mm"),
       grid_width = unit(0.25,"cm"),
       labels_gp = gpar(fontsize = 6,
                        fontface = "plain",
                        fontfamily="Helvetica"),
        title_gp=gpar(fontsize=6,
                      fontface="plain",
                      fontfamily="Helvetica"),
       title_position = "leftcenter-rot"
       ))

dev.off()


```

#### Plotting the correlation matrices 

```{r plot_cor_mat, eval = FALSE}

purrr::imap(cor_df_list,
              function(cor_df, dataset) {
                                                    pairs = combn(dataset_to_pops(dataset),2)
                                                    
                                                    all_directions = cbind(pairs, 
                                                                           rbind(pairs[2,], pairs[1,])
                                                                           )
                                                    
                                                    all_directions
                                                    
                                                    for (pair_id in 1:ncol(all_directions)) {
                                                      
                                                      plot_cor(
                                                               port_from = all_directions[1, pair_id],
                                                               port_to = all_directions[2,pair_id],
                                                               dataset= dataset,
                                                               cor_df = cor_df
                                                               )

                                                    }
                                                    
                                                    return(dataset)
                }
                                                    
)
                
```

### Plotting density plots ... 

```{r density,eval = F}

box::use(ggplot2[...],
         ggdist[...])

# feature_df %>% 
#   select()
#   pivot_longer()


feature_df %>% 
  mutate(aa_to_ea_ratio = ea_beta/aa_beta) %>% 
  mutate(class = ifelse(0.5 <aa_to_ea_ratio & aa_to_ea_ratio < 2, "portable", "non-portable")) %>% 
  mutate(across(contains("-"), ~abs(.x))) %>% 
  # mutate(aa_to_ea_ratio =ifelse(aa_to_ea_ratio>1, 1/aa_to_ea_ratio, aa_to_ea_ratio)) %>% 
  # mutate(aa_to_ea_ratio =ifelse(aa_to_ea_ratio<-1, 1/aa_to_ea_ratio, aa_to_ea_ratio)) %>% 
  ggplot(aes(x = gnomAD_AF_nfe_nwe, y = class)) +
  stat_cdfinterval(show_interval = FALSE)
  #stat_halfeye() #+
  #scale_x_continuous(trans = "log2")
  

feature_df %>% 
  mutate(aa_to_ea_ratio = ea_beta/aa_beta) %>% 
  mutate(class = ifelse(0.5 <aa_to_ea_ratio & aa_to_ea_ratio < 2, "portable", "non-portable")) %>% mutate(gnomad_diff = gnomAD_AF_amr-gnomAD_AF_nfe_nwe)  %>% 
  ggplot(aes(x=aa_to_ea_ratio, y=gnomad_diff)) +
  geom_point() + 
  geom_vline(aes(xintercept=0.5), col = "blue") +
  geom_vline(aes(xintercept=2), col = "blue") +
  scale_x_continuous(trans = "log2") +
  theme_bw()



```


## Commiting and Saving Ploting to Figures Subdir

```{r commit_plots}

wflow_dpen_commit(path="paper_figures/fig_3",
                  message = "Update Figure 3 Plots"
                  )


```

