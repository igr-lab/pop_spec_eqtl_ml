---
title: "Sup Note 1 Figures"
---


### Load required packages and utility functions 

```{r utils_fns}

# load consistent plotting functions: 
options(box.path = here::here())
box::use(code/utils/paper_plotting_functions[...])

# load git committing utility functions
box::use(code/utils/workflowr_git_utils[...])

# if changed these utility functions, commit them
wflow_dpen_commit(path = list("code/utils/workflowr_git_utils.R",
                              "code/utils/paper_plotting_functions.R"),
                  message = "Update plotting utilty functions")


```


### Replication of eQTLs close to the significance threshold




```{r, message = FALSE, class.source = 'fold-hide', warning=FALSE}

p_thres = runif(n = 10000, min = 0, max = 0.05)
p_obs = 0.99 * p_thres

# function to find prob 
prob_rep = function(p_thres, # p-value threshold for significance
                    p_obs){ # p-value observed in original study for this gene-snp pair
  
  # test statistic threshold
  test_stat_thresh = qchisq(1-p_thres,df =1)
  
  # observed test statistic in original study 
  obs_test_stat = qchisq(1-p_obs, df = 1)
  
  # prob of replicating
  return(1 - pchisq(q = test_stat_thresh,
                    ncp = obs_test_stat, 
                    df =1)
         )
  
}

library(ggplot2)
library(dplyr)

df = data.frame(p_thres = p_thres,
                p_obs = p_obs) %>% 
  mutate(prob_rep_eqtl = prob_rep(p_thres, p_obs))

plot = df %>% 
  ggplot(aes(x= p_obs, y= prob_rep_eqtl)) + 
  geom_line() + 
  custom_ggpubr_theme() + 
    labs(x = "Nominal p-value observed in study 1",
       y = "P(eQTL in study 2)",
       #title = "Probability of replicating a single eQTL at the same significance threshold",
       #subtitle = "When first study nominal p-value ~ threshold"
       ) + 
  xlim(0, 0.01) + 
  geom_hline(yintercept = 0.5, col = "red") + 
    annotate(
    "text", 
    label = "50% probability of eQTL 'porting' \n from study 1 to 2 ",
    x = 2*10^-4, 
    y = 0.5+10^-4, 
    hjust = 0,
    vjust = 0,
    colour = "red",
    size = unit(10, "pt")
  )  
  

print(plot)

  ggplot2::ggsave(filename = "paper_figures/sup/sup_note/S1_eqtl_rep_close_to_thresh_zoomed_p_obvs.pdf",
          plot = plot,
          units = "px",
          width= 750,
          device = "pdf",
          height= 562.5,
          dpi = 300,
          scale = 25/6
          )

plot = df %>% 
  ggplot(aes(x= p_thres, y= prob_rep_eqtl)) + 
  geom_line() + 
  geom_vline(xintercept = 2*10^-4, col = "black", linetype = "dashed") +
  theme_bw() + 
  labs(x = "Nominal p-value significance threshold ",
       y = "P(eQTL in study 2) ") + 
    annotate(
    "text", label = "Significance threshold for a typical eQTL study \n (FDR<~0.05, Sample n = 500)",
    x = 2*10^-4, 
    y = 0.5 + 10^-4, 
    hjust = 0,
    colour = "black",
    size = unit(10, "pt")
  )  + 
  #ylim(0.5,0.505) + 
  xlim(0, 0.01) + 
  custom_ggpubr_theme()


print(plot)

  ggplot2::ggsave(filename = "paper_figures/sup/sup_note/S1_eqtl_rep_close_to_thresh_full.pdf",
          plot = plot,
          units = "px",
          width= 750,
          device = "pdf",
          height= 562.5,
          dpi = 300, 
          scale = 25/6
          )

plot = df %>% 
  ggplot(aes(x= p_thres, y= prob_rep_eqtl)) + 
  geom_line() + 
  geom_vline(xintercept = 2*10^-4, 
             col = "black",
             linetype = "dashed") +
  theme_bw() + 
  labs(x = "Nominal p-value significance threshold ",
       y = "P(eQTL in study 2) ") + 
    annotate(
    "text", label = "Significance threshold for a typical eQTL study \n (FDR<~0.05, Sample n = 500)",
    x = 2*10^-4, 
    y = 0.5 + 5*10^-4, 
    hjust = 0,
    colour = "black",
    size = unit(10, "pt")
  )  + 
  ylim(0.5,0.505) + 
  xlim(0, 0.01) + 
  custom_ggpubr_theme()

print(plot)

  ggplot2::ggsave(filename = "paper_figures/sup/sup_note/S1_eqtl_rep_close_to_thresh_zoomed.pdf",
          plot = plot,
          units = "px",
          width= 750,
          device = "pdf",
          height= 562.5,
          dpi = 300,
          scale = 25/6
          )

```


### Replication of highly significant eQTLs

```{r}

library(RColorBrewer)

mag_order = 10^c(-10, -2, -3, -1)

p_obs = mag_order*rep(p_thres,length(mag_order))

plot = data.frame(p_thres = p_thres,
           p_obs = p_obs,
           mag_order) %>% 
  mutate(prob_rep_eqtl = prob_rep(p_thres, p_obs)) %>% 
  ggplot(aes(x= p_thres, y= prob_rep_eqtl, group = mag_order, col = factor(abs(log10(mag_order)))))+ 
  geom_line() + 
  #geom_vline(xintercept = 2*10^-4, col = "red") +
  geom_hline(yintercept = 0.8, col = "red") + 
  annotate(
    "text", label = "80% Probability of replication",
    x = 0.01, 
    y = 0.75, 
    hjust = 0,
    colour = "red",
    size = unit(10, "pt")
  ) + 
  ylim(0,1) + 
  labs(x = "P-value threshold for significance",
       y = "P(eQTL in study 2)  ",
       col = "Order of magnitude smaller than threshold",
       #title = "Probability of replicating a single eQTL at the same significance threshold",
       #subtitle = "When first study nominal p-value <<< threshold"
       ) +
  scale_color_manual(values = brewer.pal(name = "Blues", n = 7)[3:7])  + 
  custom_ggpubr_theme()

print(plot)
  ggplot2::ggsave(filename = "paper_figures/sup/sup_note/S1_eqtl_high_sig.pdf",
          plot = plot,
          units = "px",
          width= 1200,
          device = "pdf",
          height= 900,
          dpi = 300,
          scale = 25/6
          )

  
  plot = data.frame(p_thres = p_thres,
           p_obs = p_obs,
           mag_order) %>% 
  mutate(prob_rep_eqtl = prob_rep(p_thres, p_obs)) %>% 
  ggplot(aes(x= p_thres, y= prob_rep_eqtl, group = mag_order, col = factor(abs(log10(mag_order)))))+ 
  geom_line() + 
  geom_vline(xintercept = 2*10^-4, col = "black", linetype = "dashed") +
  geom_hline(yintercept = 0.8, col = "red") + 
  annotate(
    "text", label = " > 80% Probability of replication",
    x = 2*10^-4, 
    y = 0.82, 
    hjust = 0,
    colour = "red",
    size = unit(10, "pt")
  ) + 
   annotate(
    "text", label = "Significance threshold for a typical eQTL study \n (FDR<~0.05, Sample n = 500)",
    x = 2*10^-4, 
    y = 0.5 + 0.05, 
    hjust = 0,
    colour = "black",
    size = unit(10, "pt")
  )  + 
  ylim(0.5,1) + 
  xlim(0, 10^-3) + 
  labs(x = "P-value threshold for significance",
       y = "P(eQTL in study 2) ",
       col = "Order of magnitude smaller than threshold",
       #title = "Probability of replicating a single eQTL at the same significance threshold",
       #subtitle = "When first study nominal p-value <<< threshold"
       ) +
  scale_color_manual(values = brewer.pal(name = "Blues", n = 7)[3:7])  + 
  custom_ggpubr_theme()

print(plot)
  ggplot2::ggsave(filename = "paper_figures/sup/sup_note/S1_eqtl_high_sig_zoomed_in.pdf",
          plot = plot,
          units = "px",
          width= 1200,
          device = "pdf",
          height= 900,
          dpi = 300,
          scale = 25/6
          )

```


## Commiting and Saving Ploting to Figures Subdir

```{r commit_plots}

wflow_dpen_commit(path="paper_figures/sup/sup_note",
                  message = "Update Sup Note plots"
                  )


```