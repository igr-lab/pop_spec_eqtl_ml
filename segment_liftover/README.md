# LiftOver for DNAse sites

### Configuring UCSC liftOver

```
wget http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/liftOver 

chmod a+x liftOver 

```

### Filtering / preparation steps of ENCODE DNAse sites

```r

library(dplyr)

################################################################################

# Blood vessel

blood_vessel_encode = data.table::fread("data/encode_dnase_motif/blood_vessel_hint_20-footprint.bed", 
                                        header = FALSE)

names(blood_vessel_encode) = c("chr", 
                               "start", 
                               "end",
                               "source",
                               "score",
                               "strand")

test_blood_vessel = cbind("blood_vessel_encode", 
                           blood_vessel_encode) %>% 
                    filter(score > 200)

data.table::fwrite(test_blood_vessel,
                   "output/filtered_encode_dnase_motif/blood_vessel_hint_20-footprint_filt.bed", 
                   sep = "\t",
                   col.names = FALSE)

test_blood_vessel = data.table::fread("output/filtered_encode_dnase_motif/blood_vessel_hint_20-footprint_filt.bed")


################################################################################

# Lymphoblast tissue

lymphoblast_encode = data.table::fread("data/encode_dnase_motif/lymphoblast_hint_20-footprint.bed", 
                                        header = FALSE)


names(lymphoblast_encode) = c("chr", 
                               "start", 
                               "end",
                               "source",
                               "score",
                               "strand")


lymphoblast_vessel = cbind("lymphoblast_encode", 
                           lymphoblast_encode) %>% 
                     filter(score > 200)

data.table::fwrite(lymphoblast_vessel,
                   "output/filtered_encode_dnase_motif/lymphoblast_hint_20-footprint_filt.bed", 
                   sep = "\t",
                   col.names = FALSE)



################################################################################



```


### Using segment_liftover

```
pip install segment_liftover

```

We applied segment_liftover on filtered DNAse sites on slurm, for example running:

```
module load gcc/8.3.0 
module load openmpi/3.1.4 
module load gcccore/8.3.0
module load python/3.7.4

segment_liftover -l ./liftOver -i ../output/filtered_encode_dnase_motif -o ../output/liftover_encode_dnase_motif -c hg38ToHg19 -si [footprint_filt.bed] 

```
