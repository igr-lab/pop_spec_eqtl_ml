---
title: "Testing mashr lsfr vs pvalue ranking"
author: "Isobel Beasley"
date: "30/06/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Problem 

One weird problem I came across with running mashr on the Shang and Smith et al. 2020 dataset was that the number of population specific eqtls was more than the number of population shared (see below). One possible explanation for this could be that eqtls declared significant in one population do not reach the nominal significance level in the second population  - and yet



```{r intro}


# Load functions from scripts subfolder using box
options(box.path = setwd("/data/gpfs/projects/punim0586/ibeasley/pop_spec_eqtl_ml/scripts"))  

# Load functions for calling eqtls population specific or shared
box::use(calling_pop_spec_eqtl/call_pop_sec_function)

# Shang and Smith combined eqtl dataset: for how combined see combine_Shang_data.R
shang_df<-data.table::fread("/data/gpfs/projects/punim0586/ibeasley/pop_spec_eqtl_ml/data/eQTL_summary_stats/Shang_2020/combined/combined_shang_summary.csv")

# Results from running mash m.1by1: see script mashr_full_run_shang_combined.R
full_m.1by1 <- readRDS("/data/gpfs/projects/punim0586/ibeasley/pop_spec_eqtl_ml/data/mashr_results/Shang_et_al_2020/full_m.1by1.rds")

# How many population specific / shared when sig thresh of both population is 0.05

shang_df_pop_spec_orig = call_pop_specific(shang_df,
                                      snp,
                                      gene,
                                      method = "mashr_significance",
                                      sig_thresh = 0.05,
                                      sig_thresh_pop_2 = 0.05,
                                      mashr_obj = full_m.1by1)

shang_df_pop_spec_orig %>% 
  group_by(eqtl_type) %>% 
  summarise(n = n()) # more specific than shared ???? 

shang_df_pop_spec_10 = call_pop_specific(shang_df,
                                      snp,
                                      gene,
                                      method = "mashr_significance",
                                      sig_thresh = 0.05,
                                      sig_thresh_pop_2 = 0.1,
                                      mashr_obj = full_m.1by1)

shang_df_pop_spec_10 %>% 
  group_by(eqtl_type) %>% 
  summarise(n = n()) # still more specific than shared

shang_df_pop_spec_20 = call_pop_specific(shang_df,
                                      snp,
                                      gene,
                                      method = "mashr_significance",
                                      sig_thresh = 0.05,
                                      sig_thresh_pop_2 = 0.2,
                                      mashr_obj = full_m.1by1)

shang_df_pop_spec_20 %>% 
  group_by(eqtl_type) %>% 
  summarise(n = n())

shang_df_pop_spec_40 = call_pop_specific(shang_df,
                                      snp,
                                      gene,
                                      method = "mashr_significance",
                                      sig_thresh = 0.05,
                                      sig_thresh_pop_2 = 0.4,
                                      mashr_obj = full_m.1by1)

shang_df_pop_spec_40 %>% 
  select(eqtl_type) %>% 
  distinct() # finally more shared than specific

shang_df_pop_spec_50 = call_pop_specific(shang_df,
                                      snp,
                                      gene,
                                      method = "mashr_significance",
                                      sig_thresh = 0.05,
                                      sig_thresh_pop_2 = 0.5,
                                      mashr_obj = full_m.1by1)

shang_df_pop_spec_50 %>% 
  select(eqtl_type) %>% 
  distinct() # a ratio of shared to specific I expected to see

```

## Sanity check (1): does this improve if the threshold of significance in the first population is more conservative?

(i.e. as we increase the lsfr threshold from 0.05, 0.01, 0.001)

```{r}

test_ratio_thresh=function(sig_thresh_pop_1, sig_thresh_pop_2){


shang_df_pop_spec = call_pop_specific(shang_df,
                                      snp,
                                      gene,
                                      method = "mashr_significance",
                                      sig_thresh = sig_thresh_pop_1,
                                      sig_thresh_pop_2 = sig_thresh_pop_2,
                                      mashr_obj = full_m.1by1)

number_each = shang_df_pop_spec %>% 
  group_by(eqtl_type) %>% 
  summarise(n = n()) 

n_shared = number_each %>% filter(eqtl_type == "shared") %$% n

n_specific = number_each %>% filter(eqtl_type == "specific") %$% n

return(data.frame(pop_1 = sig_thresh_pop_1,
                  pop_2 = sig_thresh_pop_2,
                  ratio_shared_specific = n_shared / n_specific,
                  percent_shared_total = n_shared / (n_specific + n_shared)))

}

sapply(c(0.05, 0.1, 0.15), function(x) test_ratio_thresh(0.01, x))



shang_df_pop_spec_50_cons = call_pop_specific(shang_df,
                                      snp,
                                      gene,
                                      method = "mashr_significance",
                                      sig_thresh = 0.01,
                                      sig_thresh_pop_2 = 0.5,
                                      mashr_obj = full_m.1by1)

shang_df_pop_spec_50_cons %>% 
  group_by(eqtl_type) %>% 
  summarise(n = n()) 

```

## Sanity check (2): does lsfr correlate with supplied Shang and Smith et al. 2020 pvalues? 

## Sanity check (3): does the rank of eqtls match the ranking of pvalues in Shang and Smith