



############################################


# # # # # # # # # # # # # # # # # # # # # #

# Script intention


# Setting up comparative data from Mogil et al. into a single dataset

# combined pair-wise data is 

# Ensuring consistency, in particular between reference alleles and therefore MAF between populations


# # # # # # # # # # # # # # # # # # # # # #


############################################




########################################################



# Set up 

library(data.table)
library(dplyr)
library(dtplyr)
library(tictoc)

# Loading summary statistics from each population


# Hispanic summary statistics 

his = data.table::fread("/data/gpfs/projects/punim0586/ibeasley/data/eQTL_summary_stats/Mogil_et_al_2018/HIS_cis_eqtl_summary_statistics.txt", 
                             sep="\t")


# African American summary statistics

aa = data.table::fread("/data/gpfs/projects/punim0586/ibeasley/data/eQTL_summary_stats/Mogil_et_al_2018/AFA_cis_eqtl_summary_statistics.txt", 
                             sep="\t")


# Europea summary statistics 
ea = data.table::fread("/data/gpfs/projects/punim0586/ibeasley/data/eQTL_summary_stats/Mogil_et_al_2018/CAU_cis_eqtl_summary_statistics.txt", 
                             sep="\t")




########################################################


# according to README, test statistic is a t statistic
# hence - standard error is just  = beta / test-statistic

aa = aa %>% 
     rowwise() %>% 
     mutate(se = beta / statistic) 

ea = ea %>% 
     rowwise() %>% 
     mutate(se = beta / statistic) 

his = his %>% 
      rowwise() %>% 
      mutate(se = beta / statistic) 

aa = lazy_dt(aa)

ea = lazy_dt(ea)

his = lazy_dt(his)

# Creating pairwise African American / European dataset


# Find the set of distinct rsid / GENE id pairs that are found in both populations 

tic("Consistent snp gene")

consistent_snp_gene = Reduce("inner_join",
                      list(aa %>% 
                                  select(snps, 
                                         gene,
                                         aa_beta = beta,
                                         aa_se = se,
                                         aa_ref = ref, 
                                         aa_alt = alt),
                                ea %>% 
                                  select(snps, 
                                         gene,
                                         ea_beta = beta,
                                         ea_se = se,
                                         ea_ref = ref, 
                                         ea_alt = alt),
                           his %>% 
                             select(snps, 
                                    gene,
                                    his_beta = beta,
                                    his_se = se,
                                    his_ref = ref, 
                                    his_alt = alt)
                           ) 
                                                 )


# Remove duplicates of gene / snp pairs (caused by there sometimes being multiple alternative alleles)

toc()

tic("combine mogil swap beta")

combined_mogil_swap_beta = consistent_snp_gene %>% 
                            data.frame() %>% 
                            rowwise() %>% 
                            mutate(aa_beta = ifelse(aa_ref == ea_ref, 
                                   aa_beta, 
                                   -1*aa_beta)) %>% 
                          mutate(his_beta = ifelse(his_ref == ea_ref, 
                                    his_beta, -
                                      1*his_beta))

toc()

# combined_mogil_swap_beta = consistent_snp_gene %>% 
#   rowwise() %>% 
#   mutate(aa_beta = ifelse(aa_ref == ea_ref, aa_beta, -1*aa_beta)) %>% 
#   mutate(his_beta = ifelse(his_ref == ea_ref, his_beta, -1*his_beta))


fwrite(combined_mogil_swap_beta,
            quote=FALSE, 
            sep='\t',
            "/data/gpfs/projects/punim0586/ibeasley/data/eQTL_summary_stats/Mogil_et_al_2018/combined/combined_mogil_v1.tsv")



# Then for these which have the same rsid / GENE 
# which have the same reference allele
# ref_consistent_snp_gene = consistent_snp_gene %>% 
#                            as.data.frame() %>% # cannot use rowwise with dtplyr
#                            rowwise() %>% 
#                            mutate(
#                            same_ref = ifelse(ref == e_ref & alt == e_alt, TRUE, FALSE))
#     
# 
# 
# lazy_dt(ref_consistent_snp_gene) %>% 
#   group_by(same_ref) %>% 
#   summarise(n = n()) %>% 
#   as.data.frame()
# 
# 
# 
# # is the discrepancy in reference allele just coming from swaping of reference / alternate allele? 
# lazy_dt(ref_consistent_snp_gene) %>% 
#   filter(same_ref == FALSE) %>% 
#   as.data.frame() %>% 
#   rowwise() %>% 
#   mutate(revs = ifelse(ref == e_alt & alt == e_ref, TRUE, FALSE)) %>% 
#   group_by(revs) %>% 
#   summarise(n = n())
# 
# # all changes to ref / alternative allele are just switches between populations
# 
# 
# # want to make sure that reference and alternative alleles are same across populations
# 
# # so to combine EA / AA 
# # make EA the "reference" population to determine the reference / alternative allele
# 
# 
# # according to README, test statistic is a t statistic
# # hence - standard error is just  = beta / test-statistic
# 
# consistent_aa = inner_join(aa, 
#                            ref_consistent_snp_gene) %>% 
#   as.data.frame() %>% 
#   rowwise() 
#   #mutate(aa_af = ifelse(same_ref == TRUE, af, 1-af)) %>%  # no allele frequency column in this dataset - need to get access
#   mutate(se = beta/statistic)
#   #select(-af) %>% 
#   #filter(is.na(allele0)==F|is.na(allele1)==F)  %>% 
# 
#     consistent_aa %>% select(aa_alt = alt,
#          aa_ref = ref,
#          aa_beta = beta,
#          aa_se = se,
#          aa_pvalue = pvalue,
#          snps,
#          gene)
# 
# 
# combined_Mogil_ea_aa = inner_join(ea %>% 
#                               select(ea_ref = ref,
#                                      ea_alt = alt,
#                                      #ea_af = af,
#                                      ea_beta = beta,
#                                      ea_se = se,
#                                      ea_pvalue = pvalue,
#                                      snps,
#                                      gene) , 
#                             consistent_aa)
