---
title: "combine_mogil_analysis"
author: "Isobel Beasley"
date: "21/06/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```



```{r message=FALSE, warning=FALSE}

# set up

library(data.table)
library(dplyr)
library(dtplyr)
library(tidyr)
library(tictoc)
library(magrittr)

# Set working directory of data to load summary statistics

setwd("/data/gpfs/projects/punim0586/ibeasley/pop_spec_eqtl_ml/data")

# Loading summary statistics from each population


# Hispanic population summary statistics 

his = data.table::fread("eQTL_summary_stats/Mogil_2018/HIS_cis_eqtl_summary_statistics.txt", sep="\t")


# African American population summary statistics

aa = data.table::fread("eQTL_summary_stats/Mogil_2018/AFA_cis_eqtl_summary_statistics.txt", sep="\t")


# European American population summary statistics 

ea = data.table::fread("eQTL_summary_stats/Mogil_2018/CAU_cis_eqtl_summary_statistics.txt", sep="\t")

# Use dtplyr to speed up dplyr code

aa = dtplyr::lazy_dt(aa)

ea = dtplyr::lazy_dt(ea)

his = dtplyr::lazy_dt(his)

```

## Are there multiple reference alleles within a single population? 

```{r}

# Are the any snps with multiple reference alleles within a population

 # African American Population
  multi_ref_aa = aa %>% 
   group_by(snps) %>% 
   select(snps, ref) %>% 
   distinct() %>% 
   summarise(n = n()) %>% 
   filter(n > 1) %>% 
   as.data.frame()

# number of snps with multiple reference alleles 
  
  nrow(multi_ref_aa) # 0 (no snps have multiple reference alleles in the African American population)

# European American Population
  multi_ref_ea = ea %>% 
   group_by(snps) %>% 
   select(snps, ref) %>% 
   distinct() %>% 
   summarise(n = n()) %>% 
   filter(n > 1) %>% 
   as.data.frame()
  
  # number of snps with multiple reference alleles 
  
  nrow(multi_ref_ea) # 0 (no snsps have multiple reference alleles in the European American population)
  
# Hispanic Population
   multi_ref_his = his %>% 
   group_by(snps) %>% 
   select(snps, ref) %>% 
   distinct() %>% 
   summarise(n = n()) %>% 
   filter(n > 1) %>% 
   as.data.frame()
  
  # number of snps with multiple reference alleles 
  
  nrow(multi_ref_his) # 0 (no snps have multiple reference alleles in the Hispanic population)
```

So, no there are not multiple reference alleles within a single population. 

## Are there multiple alternative alleles within a single population? 

```{r}
  
# Are the any snps with multiple alternative alleles within a population
  
  # African American
  multi_alt_aa = aa %>% 
   group_by(snps) %>% 
   select(snps, alt) %>% 
   distinct() %>% 
   summarise(n = n()) %>% 
   filter(n > 1) %>% 
   as.data.frame()
  
  # number of snps with multiple alternative alleles 
  
  length(unique(multi_alt_aa$snps))
  
  # which snps have multiple alternative alleles in the african american population
  
  multi_alt_aa_snps = unique(multi_alt_aa$snps)
  
  # European American 
   multi_alt_ea = ea %>% 
   group_by(snps) %>% 
   select(snps, alt) %>% 
   distinct() %>% 
   summarise(n = n()) %>% 
   filter(n > 1) %>% 
   as.data.frame()
   
  # number of snps with multiple alternative alleles 
  
  length(unique(multi_alt_ea$snps))
  
  # which snps have multiple alternative alleles in the european american population
   
   multi_alt_ea_snps = unique(multi_alt_ea$snps)
  
  # Hispanic
   
   multi_alt_his = his %>% 
   group_by(snps) %>% 
   select(snps, alt) %>% 
   distinct() %>% 
   summarise(n = n()) %>% 
   filter(n > 1) %>% 
   as.data.frame()

  # number of snps with multiple alternative alleles 
  
  length(unique(multi_alt_his$snps))
  
  # which snps have multiple alternative alleles in the hispanic population
   
   multi_alt_his_snps = unique(multi_alt_his$snps)


   # Example of snps with multiple alternative alleles 
   
   multi_alt_his_snps[1:10]
   
```

Yes, there are a number of snps which have multiple alternative alleles.

## Do the snps with multiple alternative alleles, have multiple alternative alleles in the other populations?

```{r crossover_multiple_alternative_alleles}

# Total number of snps which have multiple alternative alleles

length(Reduce("union", list(multi_alt_his_snps,
                                  multi_alt_ea_snps,
                                           multi_alt_aa_snps)))

# Is there any overlap between what snps have multiple alternative alleles in 

any(multi_alt_aa_snps %in% multi_alt_ea_snps)

any(multi_alt_aa_snps %in% multi_alt_his_snps)

any(multi_alt_ea_snps %in% multi_alt_his_snps)


# Number of snps  which have multiple alternative alleles in all populations?

length(Reduce("intersect", list(multi_alt_his_snps,
                                  multi_alt_ea_snps,
                                           multi_alt_aa_snps)))


# Any snps have multiple alternative alleles in a single populations?

length(
       Reduce("setdiff", list(multi_alt_his_snps,
                     multi_alt_ea_snps,
                     multi_alt_aa_snps)) 
) 


```

# Example of snps which have multiple reference alleles: 

```{r example_snps_with_multi_reference_alleles}

# Combine all mogil single population datasets 
# joining by snp and gene information
# and ensuring statistics / beta / pvalues are labeling to point out which population 
# they are calculated from (by first renaming these columns)

combine_mogil = Reduce("full_join",
                      list(aa %>%
                              rename(aa_beta = beta,
                                         aa_statistic = statistic,
                                         aa_pvalue = pvalue,
                                         aa_FDR = FDR,
                                         aa_ref = ref, 
                                         aa_alt = alt),
                                ea %>% 
                                  rename(ea_beta = beta,
                                         ea_statistic = statistic,
                                         ea_pvalue = pvalue,
                                         ea_FDR = FDR,
                                         ea_ref = ref, 
                                         ea_alt = alt),
                           his %>% 
                             rename(his_beta = beta,
                                    his_statistic = statistic,
                                    his_pvalue  = pvalue, 
                                    his_FDR = FDR,
                                    his_ref = ref, 
                                    his_alt = alt)
                           ) 
                                                 )



# Extract information on snps for which there are multiple alternative alleles in at least one population

multi_alt_mogil = combine_mogil %>% 
  filter(snps %in% multi_alt_his_snps | snps %in% multi_alt_aa_snps | snps %in% multi_alt_ea_snps )

head(multi_alt_mogil) %>% as.data.frame()

set.seed(701)
# 
# # see pattern of these snps across populations



# Importantly, illumina arrays are suspect to strand issues
# Are these patterns caused by this i.e. A/T swappage
# or G/C swappage? 

test = combine_mogil %>% 
  filter(snps %in% multi_alt_his_snps) %>% 
   as.data.frame() %>% 
   rowwise() %>% 
   select(snps, his_alt) %>% 
   distinct() %>% 
   arrange(snps) %>% 
   summarise(n = n())

summary(test$n)


alt_id = sapply(test$n, function(x) LETTERS[1:x])

alt_his_test = cbind(alt_id,
             combine_mogil %>% 
              filter(snps %in% multi_alt_his_snps) %>% 
              as.data.frame() %>% 
             rowwise() %>% 
             select(snps, his_alt) %>% 
             distinct() %>% 
             arrange(snps)) %>%  
             pivot_wider(id_cols = snps, names_from = alt_id, values_from = his_alt) 

head(alt_his_test)

alt_his_test = alt_his_test %>% 
  mutate(p_strand_issue = ifelse(A == "A" & B == "T" | A == "T" & B == "A" | A == "C" & B == "G" | A == "G" & B == "C" , TRUE, FALSE))

sum(alt_his_test$p_stand_issue, na.rm = TRUE)

# example_aa_snps = sample(multi_alt_aa_snps, size = 2)
# 
# example_ea_snps = sample(multi_alt_ea_snps, size = 2)
# 
# example_his_snps = sample(multi_alt_his_snps, size = 2)
# 
#  for(i in 1:2){
# 
#   combine_mogil %>%
#     filter(snps = example_aa_snps[1]) %>%
#     head() %>%
#     as.data.frame() %>%
#     print()
# 
#   combine_mogil %>%
#     filter(snps = example_ea_snps[i]) %>%
#     head() %>%
#     as.data.frame() %>%
#     print()

#   combine_mogil %>%
#     filter(snps = example_his_snps[i]) %>%
#     head() %>%
#     print()
# 
# }


```

# Are these snps with multiple alternative alleles part of any eQTLs (i.e. gene / snp pairs with FDR < 0.05)? 

In particular, if we removed them would they impact the distribution of FDR, or are they just a random sample of all gene-snp pairs?

```{r fdr_multi_alternative_alleles}

min_FDR_multi_alt_allele = multi_alt_mogil %>% 
                as.data.frame() %>% 
                rowwise() %>% 
                mutate(FDR = min(his_FDR, ea_FDR, aa_FDR, na.rm = TRUE)) %$%
                FDR

full_data_min_FDR = combine_mogil  %>% 
                as.data.frame() %>% 
                rowwise() %>% 
                mutate(FDR = min(his_FDR, ea_FDR, aa_FDR, na.rm = TRUE)) %$%
                FDR

summary(min_FDR_multi_alt_allele)


hist(min_FDR_multi_alt_allele, 
                              xlim = c(0,1))


summary(full_data_min_FDR)

hist(sample(full_data_min_FDR, size = 2000))


```

## (For snps with a single alternative allele in all populations): Are the reference and alternative allele just swapped between populations? 

```{r single_alternative_allele}
# Extract only snps for which there is only one alternative allele in every population
single_alt_allele = anti_join(combine_mogil,
                                            multi_alt_mogil)


# Set Europeans as the 'reference' allele population
# compare reference and alternative alleles in aa and his populations
single_alt_allele_aa  = single_alt_allele %>% 
  dplyr::select(snps, #select columns which identify the snp,
         ea_ref, #and the reference / alternative snp in each population
         ea_alt, 
         aa_ref, 
         aa_alt) %>% 
  distinct() %>% 
  as.data.frame() %>% 
  filter(!is.na(ea_ref)|!is.na(ea_alt)|!is.na(aa_ref)|is.na(aa_alt)) %>% 
  rowwise() %>% # sample_ref_alt_aa (logical column = does this snp have the same ref / alt allele in ea and aa pops?)
  mutate(same_ref_alt_aa = ifelse(ea_ref == aa_ref & ea_alt == aa_alt, TRUE, FALSE)) %>%
  mutate(ref_alt_swapped_aa = ifelse(ea_ref == aa_alt & ea_alt == aa_ref, TRUE, FALSE)) 

# compare reference and alternative alleles in aa and his populations
single_alt_allele_his  = single_alt_allele %>% 
  dplyr::select(snps, #select columns which identify the snp,
         ea_ref, #and the reference / alternative snp in each population
         ea_alt, 
         his_ref,
         his_alt) %>% 
  distinct() %>% 
  as.data.frame() %>% 
  rowwise() %>% # sample_ref_alt_aa (logical column = does this snp have the same ref / alt allele in ea and aa pops?)
  filter(!is.na(ea_ref)|!is.na(ea_alt)|!is.na(his_ref)|is.na(his_alt)) %>% 
  mutate(same_ref_alt_his = ifelse(ea_ref == his_ref & ea_alt == his_alt, TRUE, FALSE)) %>%
  mutate(ref_alt_swapped_his = ifelse(ea_ref == his_alt & ea_alt == his_ref, TRUE, FALSE))

head(single_alt_allele_his)

head(single_alt_allele_aa)

# Total number of snps with single alternative allele (in each population)  

nrow(single_alt_allele_aa)

# Check NA 

sum(is.na(single_alt_allele_aa$same_ref))

# Same alternative allelle 

sum(single_alt_allele_aa$same_ref_alt_aa, na.rm = TRUE)

# Total number of these snps which have different reference and alternative allele in aa / ea

nrow(single_alt_allele_aa)-sum(single_alt_allele_aa$same_ref_alt_aa, na.rm = TRUE)
  
# Total number of snps with single alternative allele
# for which swapping the reference alternative allele is the difference 

sum(is.na(single_alt_allele_aa$same_ref, na.rm = TRUE))  

single_alt_allele_aa = single_alt_allele_aa %>% filter(!same_ref_alt)

sum(single_alt_allele_aa$ref_alt_swapped_aa, na.rm = TRUE)  




# Total number of these snps which have different reference and alternative allele in his / ea

nrow(single_alt_allele_his)-sum(single_alt_allele_his$same_ref_alt_his, na.rm =TRUE)
    
  # Total number of snps with single alternative allele
# for which swapping the reference alternative allele is the difference 

single_alt_allele_his = single_alt_allele_his %>% filter(!same_ref_alt)

sum(single_alt_allele_his$ref_alt_swapped_his, na.rm = TRUE)        
    
```


## Session Information

```{r}

sessionInfo()

```

