#!/usr/bin/env Rscript --vanilla

suppressPackageStartupMessages(require(optparse)) 



##################### Intention: #################################



##################### Optparser options: ###########################

option_list = list(

  
# coloc results
make_option(c("-c", "--coloc_results"), 
            type="character", 
            help="mashr_results", 
            metavar="character"),

# Portability thresholds settings
make_option(c("-f", "--caus_thresh"), 
            type = "numeric", 
            default=0.95,
            help ="Posterior probability of casuality threshold for calling significance in the population of interest"),
make_option(c("-r","--prob_ratio"), 
            type = "numeric", 
            default=0.5,
            help = "Threshold for prob of sharing, given casuality"),

# defining portability settings
make_option(c("--p1"), 
            type = "character", 
            help = "What are the dataset/s names which correspond 
              to population/s of interest (the population/s we are using 
              for declaring 'population-specific')"),
make_option(c("--p2"), 
            type = "character", 
            help = "What are the dataset/s names corresponding to the contrast population/s)"),
make_option(c("--first_listed_pop"), 
            type = "character", 
            help = "What is population is listed as the first in the provided saved data?"),


# output / saving information settings
make_option(c("-s", "--save"), 
            type = "character", 
            default=NULL, 
            help = "What to save the output data frame as"),
make_option(c("--sep_out"), 
            type = "character", 
            default="\t",
            help = "sep for saving output dataframe"),
make_option(c("--plots"), 
            type = "logical", 
            default = TRUE,
            help = "Should we save relevant plots (upset plot, and sumamry tables)? Default = TRUE")
); 


opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

renv::load(here::here())


###############################################################

coloc_results = readRDS(opt$coloc_results)



# extract the names of the specific populations
port_from_pops = strsplit(opt$p1, ",")[[1]]

# extract the names of the shared populations
port_to_pops = strsplit(opt$p2, ",")[[1]]



# H1 + H3 + H4 (prob casual region association)
# Prob shared casual association 

############ Step 1 Significant in port from pop ############



coloc_results %>% 
  filter(if_any(contains("H4")))