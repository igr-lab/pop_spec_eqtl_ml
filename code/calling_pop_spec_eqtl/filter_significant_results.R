#!/usr/bin/env Rscript --vanilla

suppressPackageStartupMessages(require(optparse)) 



##################### Intention: #################################



# Script to call eQTLs significant in a single population (from a single df)

# using mashr results - 

# And produce a filtered df of just these significant eQTLs




## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## #



##################### Optparser options: ###########################

option_list = list(
  make_option(c("-r", "--mashr_results"), 
              type="character", 
              default=NULL, 
              help="mashr_results", 
              metavar="character"),
  make_option(c("-s", "--save"), 
              type = "character", 
              default=NULL, 
              help = "What to save the output data frame as"),
  make_option(c("-i", "--input_dataframe"), 
              type = "character", 
              default=NULL,
              help = "Input dataframe"),
  make_option(c("-l", "--lfsr"), 
              type = "numeric", 
              default=0.01,
              help ="lfsr threshold for calling significance in the population of interest"),
  make_option(c("--p1"), 
              type = "character", 
              default=NULL,
              help = "What are the dataset/s names which correspond 
              to population/s of interest (the population/s we are using 
              for declaring 'population-specific')"),
  # make_option(c("--rep_p1"), 
  #             type = "character", 
  #             default=NULL),
  make_option(c("--sep_in"), 
              type = "character", 
              default="\t",
              help = "sep for reading in input_dataframe "),
  make_option(c("--sep_out"), 
              type = "character", 
              default="\t",
              help = "sep for saving output dataframe"),
  make_option(c("--gene_column"), 
              type = "character", 
              default="gene",
              help = "Name of the gene column in the input_dataframe"),
  make_option(c("--snp_column"), 
              type = "character", 
              default="snp",
              help = "Name of the snp column in the input_dataframe"),
  make_option(c("-m", "--majority_rules"), 
              type="logical", 
              default=TRUE, 
              help="Should we define shared as being found in a found in a majority 
                    of the secondary populations? *set = TRUE). 
              Else shared is defined as being shared / found in any secondary population dataset")
); 


opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);



################ Set up  ########################


# extract the names of the populations to declare significance for
spec_pops = strsplit(opt$p1, ",")[[1]]

# if (length(spec_pops)>1){
#   
#   if(is.null(opt$rep_p1)){
#     
#   stop("If there is more than one specific population dataset (--p1), then
#        you need to specify a representative population, and set this under flag --rep_p1")
#     
#   }
# }



# read in the mashr results
mashr_results = readRDS(opt$mashr_results)

# renv::load(here::here())

# required packages)
box::use(magrittr[...])




############ Get significant eQTLs (from the primary population) ###########




# Which results are significant in the primary ('specific') populations
sig_res = mashr_results$lfsr %>%
          as.data.frame() %>%
          dplyr::filter(dplyr::if_any(dplyr::all_of(spec_pops), ~.x<opt$lfsr))


if(length(spec_pops) > 1){
  
sig_res = sig_res %>% 
          dplyr::mutate(
                        dplyr::across(dplyr::all_of(spec_pops),
                        ~.x<opt$lfsr)
                        ) %>% 
          dplyr::mutate(n_spec_datasets = rowSums(dplyr::across(all_of(spec_pops)), 
                                                 na.rm = T)
                        )

  if(opt$majority_rules){ #if declaring eQTLs shared by majority rules (shared if)
  # the same effect direction in the majority of 'secondary' / 'shared' populations
  
  
  total_spec_pops = length(spec_pops)
  
  n_for_majority = ceiling(total_spec_pops * 0.5)
  
  
  
  ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
  
  
  sig_res =  sig_res %>% 
             dplyr::filter(n_spec_datasets >= n_for_majority)
  
  } else {
   
    sig_res =  sig_res %>% 
               dplyr::filter(n_spec_datasets >= 1) 
  
  }

         
}

 sig_res = sig_res %>%
           tibble::rownames_to_column("gene_snp") %>% 
           tidyr::separate(., col = "gene_snp", into = c("gene", "snp")) %>% 
           dplyr::select(gene, snp)



## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##







################# Saving output files ############################

# saving output files

if(
    !is.null(opt$save)
                       ){
  

    
  output_dataframe = sig_res 
  
  
  if(
     !is.null(opt$input_dataframe) 
                                    ){
    
    

        
    # if an input data.frame is provided 
    # add and save results with the input data.frame
    
    input_dataframe = read.table(opt$input_dataframe, 
                                 sep = opt$sep_in, 
                                 header = TRUE)
    
    
    #remove class column where required
    input_dataframe = input_dataframe %>% dplyr::select(-any_of("Class"))
    
    
      
    # if rownames are gene_ids snp - convert row names to columns  
    if(is.na(
                      suppressWarnings(
                                        as.numeric(rownames(input_dataframe)[1])))  ) {
     
      input_dataframe = input_dataframe %>% 
                        tibble::rownames_to_column("gene_snp")
      
       
    } else if (
                ("gene_snp" %in% names(input_dataframe))) {
      
      
      output_dataframe = output_dataframe %>%
                         tidyr::separate("gene_snp", into = c("gene", "snp")) %>% 
                          plyr::rename(.,
                     c("gene"= opt$gene_column,
                       "snp" = opt$snp_column))
    }
    
    

    
    output_dataframe = suppressMessages(
                                         dplyr::left_join(output_dataframe,
                                                          input_dataframe
                                        
      )
    )
    
    
  }  
  
  message("\n Saving output dataframe ...")
  
  print(
        head(
             output_dataframe %>% 
             dplyr::select(dplyr::any_of(1:10) )
                                                )
                                                 )
  

  
  if("gene_snp" %in% names(output_dataframe)){
    
    
    output_dataframe = output_dataframe %>% 
                       tibble::column_to_rownames("gene_snp") %>%
                       dplyr::distinct()
    
    
    
    write.table(output_dataframe,
                opt$save,
                sep = opt$sep_out)

          
      
  } else {
    
  data.table::fwrite(output_dataframe,
                     opt$save,
                     sep = opt$sep_out)
    

  }
}