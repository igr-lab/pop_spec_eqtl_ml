

# File taken 8/10/2021

# from :https://sapac.support.illumina.com/downloads/humanht-12_v4_product_file.html

library(dplyr)
library(magrittr)

illumina_probe_info = data.table::fread("data/illumina_array_info/HumanHT-12_V4_0_R2_15002873_B.txt", skip = 6)

illumina_probe_info = illumina_probe_info %>% 
                      as.data.frame() %>% 
                      dplyr::select(Probe_Id, 
                                    RefSeq_ID, 
                                    Symbol, 
                                    Transcript, 
                                    Chromosome, 
                                    Probe_Coordinates, 
                                    Probe_Start, 
                                    Probe_Sequence) %>% 
                     #filter(Probe_Id == "ILMN_1658992"| Probe_Id == "ILMN_2105308") %>% 
                     tidyr::separate_rows(., 'Probe_Coordinates',sep="[:]")  %>% 
                     tidyr::separate("Probe_Coordinates", into = c("probe_coord_start", "probe_coord_end"), sep = "[-]")


fairfax_2 = fairfax %>% filter(type == "SNP")

fairfax_2 %>% head()

box::use(code/feature_extraction)

fairfax_location = feature_extraction$id_to_granges(fairfax_2 %>% 
                                                    as.data.frame() %$% 
                                                    gene_id, 
                                                    type = "Gene")

fairfax = data.table::fread("data/eqtl_summary_stats/Mogil_2018/similar_eqtl_studies/Fairfax_2014_microarray_monocyte_naive.all.tsv")

# rename fairfax columns to identify they are from fairfax (important for combining datasets later)
# + remove unnecessary columns for this analysis 
fairfax = lazy_dt(fairfax) %>% 
  filter(type == "SNP") 