
# Utils files:

| Files | Description |
| --- | --- |
| **workflowr_wrapper_functions.R** | Wrapper functions around workflowr functions (```wflow_build``` and ```wflow_publish```) to enable the use of folders other than analysis for storing, building and publishing .Rmd scripts for the workflowr page of this project. This file has been set up with roxygen, and can be used with the box R package, by running ```box::use(code/workflowr_wrapper_functions) ``` |
| **workflowr_wrapper_functions.R**| Function for submitting slurm jobs from rmarkdown files, and displaying information about the input and output files for this job - in a collapsible markdown format. |
| **plotting_results_functions.R** | Functions for plotting, including simple default custom ggplot settings, plotting functions for comparing two different importance ranks, grouping features etc. | 
