#!/usr/bin/env Rscript --vanilla

renv::load()

suppressPackageStartupMessages(require(optparse)) 


###############################################################################


# Intention

# Use the extract_features function from the command line using optparser

# to extract features of eQTLs using curated and publically avaliable datasets


##############################################################################

# Optparser options

option_list = list(
  make_option(c("-s", "--save"), 
              type = "character", 
              default=NULL,
              help = "What + where to save the produced features data frame"),
  make_option(c("-d", "--data_frame"), 
              type = "character",
              help = "The data frame to feed to extract_features (to add the features to)"),
  make_option(c("--sep"), 
              type = "character", 
              default="\t",
              help = "sep for reading in data table into data.table::fread"),
  make_option(c("--gene_column"), 
              type = "character", 
              default="gene",
              help = "Name of the gene column in the input_dataframe"),
  make_option(c("--snp_column"), 
              type = "character", 
              default="snp",
              help = "Name of the snp column in the input_dataframe"),
  make_option(c("--ref_column"), 
              type = "character", 
              default="ref",
              help = "Name of the column identifying the reference allele in the input_dataframe"),
  make_option(c("--alt_column"), 
              type = "character", 
              default="alt",
              help = "Name of the column identifying the alternative allele in the input_dataframe"),
  make_option(c("-t", "--tissue"), 
              type = "character", 
              default=NULL,
              help = "Name of tissue for this dataset (Only designed to work with:
              wb for whole blood, lcl, for lymphoblastoid, mono for naive monocytes. Used to work out
              which DNAse I hypersensitive sites file, within GTEx tissue expression metrics file etc."),
  make_option(c("--go_grouping"), 
              type = "character", 
              default="immune",
              help = "Any particular grouping of GO terms (so far only immune 
              for immune related GO terms is avaliable)"),
  make_option(c("--flash_factors"), 
              type = "character", 
              default="None", 
              help = "Number of flash factors (capturing GTEx cross tissue gene expression) to select", 
              metavar="number"),
  make_option(c("--pca_components"), 
              type = "character", 
              default="1,2,3,4,5,6,7,8,9,10",
              help = "Number of PCA components (capturing GTEx cross tissue gene expression) to select", 
              metavar="number"),
  make_option(c("--thous_genome_maf_pops"), 
              type = "character", 
              default="None", 
              help = "Which thousand genome populations to extract minor allele frequency information from?
              Possible options; any combination of: EUR_AF,AFR_AF,AMR_AF,EAS_AF,SAS_AF"),
  make_option(c("--gnomad_maf_pops"), 
              type = "character", 
              default="AF_afr,AF_amr,AF_eas,AF_asj,AF_fin,AF_nfe,AF_nfe_nwe,AF_nfe_est,AF_nfe_seu",
              help = "Which gnomAD population/s to extract minor allele frequency information from?"),
  make_option(c("--window_size"),
              type = "character", 
              default="100",
              help = "Window size around the SNP to extract features around (e.g. for % nucleotide). 
              Can be multiple window sizes if required.", 
              metavar="number"),
  make_option(c("--gene_features"), 
              type = "character", 
              default="default_set",
              help = "Which gene features to select"),
  make_option(c("--snp_features"), 
              type = "character", 
              default="default_set",
              help = "Which snp features to select"),
  make_option(c("--combined_features"), 
              type = "character", 
              default="default_set",
              help = "Which combined features to select"),
  make_option(c("--testing"), 
              type = "logical", 
              default = FALSE, 
              help = "Test the pipeline by running it on a sample of 500 eQTLs without saving results?"),
  make_option(c("--prep_for_ml_pipeline"), 
              type = "logical", 
              default =TRUE, 
              help = "Is the data.frame produced by this pipeline supposed to be used in the machine learning pipeline? 
              (Changes the format by which it is saved!)"),
  make_option(c("--effect_direction"), 
              type = "logical", 
              default =TRUE, 
              help = "Use the effect size column of the input data frame,
              (where effect size column is avaliable - it uses the column provided to effect_direction_col), 
              to produce an effect direction column."),
  make_option(c("--effect_direction_col"),
              type = "character",
              help = "The name of the effect size column to extract effect direction from, 
              required if effect_direction is true")
);

opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);


###### Set up / get required feature extraction functions #####


# required feature extraction functions

renv::load(here::here())

box::use(../feature_extraction)

box::use(dplyr[...])


## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 




########## Set up extract_features arguments #################



############ Minor Allele Frequency ##################


if(tolower(opt$thous_genome_maf_pops) != "none" |
   tolower(opt$thous_genome_maf_pops) != "null"){
  

thous_genome_maf_pops = unlist(strsplit(opt$thous_genome_maf_pops, ","))


} else {
  
thous_genome_maf_pops = NULL 

}

if(tolower(opt$gnomad_maf_pops) != "none"|
   tolower(opt$gnomad_maf_pops) != "null"){

gnomad_maf_pops = unlist(strsplit(opt$gnomad_maf_pops, ","))


} else {

gnomad_maf_pops = NULL

}


###### Cross tissue measurements - flash factors and PCA components #########


if(tolower(opt$flash_factors) != "none"){
  
flash_factors = c(scan(text=opt$flash_factors, sep=",", quiet=TRUE))

} else {
  
flash_factors = NULL  

} 


if(tolower(opt$pca_components) != "none"){ 
  
pca_components = scan(text=opt$pca_components, sep=",", quiet=TRUE)

} else {
  
pca_components = NULL 

}



############## Window size ##################




window_size = scan(text=opt$window_size, sep=",", quiet=TRUE)




######## Set of SNP features ###############



if(opt$snp_features == "default_set"){
  
  message("\n Using the default set of SNP features...")
  
  
  snp_features = c("genomicdistributions_location",
                   "gnomAD_maf",
                   "ld_score_cross_pop_measures",
                   "CADD",
                   "linsight",
                   "fitCons",
                   "percent_di_nucleotide_win",
                   "dis_to_TSS",
                   "win_GC_content")
  
  
}else{
  
  
 snp_features = strsplit(x=opt$snp_features, split=",")[[1]]
 
 
}


message("SNP features: \n", cat(snp_features, sep = ", "))


########### Set of Gene features  #####################



if(opt$gene_features == "default_set"){
  
  message("\n Using default complete set of gene features:")
  
  gene_features = c("gene_biotype", 
                    "gene_duplication_status",
                    "goslim_goa_accession",
                    "phastCons",
                    #"kegg_enzyme",
                    "gnomAD_cds_length",
                    "oe_lof_upper",
                    "oe_syn_upper",
                    "oe_mis_upper",
                    "p_afr",
                    "p_amr",
                    "p_asj",
                    "p_nfe",
                    "p_eas",
                    "p_sas",
                    "p_fin",
                    "p_oth",
                    "phyloP",
                    "immport_annotation",
                    "innatedb_annotation",
                    "interpro_annotation",
                    "percent_amino_acid",
                    "percent_gc", 
                    "percent_di_nucleotide",
                    "gtex_expression_measures", 
                    "gtex_tissue_breadth_no_brain")
  
  
  
  
}else{
  
  gene_features = strsplit(x=opt$gene_features, split=",")[[1]]
  
}   


message("Gene features: \n", cat(gene_features, sep = ", "))

######### Set of combined features ############

if(opt$combined_features == "default_set"){
  
  
  combined_features = "dist_esnp_egene"
  combined_features = NULL
  
}else if(tolower(opt$combined_features) == "none" | 
         tolower(opt$combined_features) == "null"){
  
  
  combined_features = NULL
  
  
}else {
  
  combined_features = c(scan(text=opt$combined_features, sep=",", quiet=TRUE))
  
}   


######## What tissue to use to find transcription / protein features ##########



# if is niave monocyte (i.e. opt$tissue = "mono") then use whole blood to work out

# and don't extract transcription features from gtex (gtex doesn't have monocyte expression features)

if(!is.null(opt$tissue)){
  
  
  if(opt$tissue == "mono"){
    
    
    gtex_tissue = NULL
    
    transcript_selection_tissue = "wb"
    
    
  }else{
    
    gtex_tissue = opt$tissue
    
    transcript_selection_tissue = opt$tissue
    
  }
    
  
}else{
  
  
  gtex_tissue = opt$tissue
  
  transcript_selection_tissue = opt$tissue
  
  
}



####### Set up data frame to which to features to ###########



df_to_add_features = data.table::fread(opt$data_frame,
                                       sep = opt$sep)


## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 


if(
   ("gene_snp" %in% names(df_to_add_features)) |
   ("V1" %in% names(df_to_add_features))
   ){
  
  df_to_add_features = df_to_add_features %>% 
                       dplyr::rename("gene_snp" = "V1") %>% 
                       tidyr::separate("gene_snp", into = c("gene", "snp"))
  
}   


## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

if(opt$prep_for_ml_pipeline){
  
  message("Sampling down to one eQTL per eGene")
  
  set.seed(300)
  
  df_to_add_features = df_to_add_features %>% 
                       dplyr::group_by(gene) %>% 
                       dplyr::slice_sample(n = 1) %>% 
                       dplyr::ungroup()
  
}

if(opt$testing){
  
  message("Testing the pipeline on a random sample of 1,000 eQTLs")
  
  set.seed(156)
  
  df_to_add_features = df_to_add_features %>% 
                       dplyr::sample_n(500)

}


####### Apply extract features function to get requested functions #########


message("\n ... Adding features to ... ")


message(" ")
print(utils::head(df_to_add_features %>% dplyr::select(dplyr::any_of(1:10)) ))
message("Dimensions: ", nrow(df_to_add_features)," x ", ncol(df_to_add_features))
message(" ")


# Extract features using the feature_extraction function
output_df = feature_extraction$extract_features(data = df_to_add_features,
                                                SNP_column = snp,
                                                GENE_column = gene,
                                                ref_column = ref,
                                                alt_column = alt,
                                                snp_features = snp_features,
                                                gene_features = gene_features,
                                                gtex_tissue_exp_pca_components = pca_components,
                                                gtex_tissue_exp_flash_factors = flash_factors, 
                                                transcript_selection_tissue = transcript_selection_tissue, 
                                                gtex_tissue = gtex_tissue, 
                                                combined_features = combined_features, # "dist_esnp_egene", 
                                                encode_dhs_tissue = opt$tissue,
                                                gnomAD_maf_pop = gnomad_maf_pops, 
                                                thous_genome_pop = thous_genome_maf_pops,
                                                go_grouping = opt$go_grouping,
                                                window_size = window_size)



## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 





## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 


######## Add effect size direction features #############

if(opt$effect_direction & any(grepl("beta", names(output_df)))){
  
  
  if(is.null(opt$effect_direction_col)){
    
    message("No effect direction column name was provided, 
            so no effect direction column was made")
  } else {
  output_df = output_df %>% mutate(across(opt$effect_direction_col, 
                                          ~sign(.x), 
                                          .names = "effect_direction"
                                          )
                                   )
  }
}


## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

################# Format output  data.frame #####################

# Format out data.frame
# gene-snp pairs become instance identifiers

if(opt$prep_for_ml_pipeline){
  
  output_df = output_df %>%
    tidyr::unite("gene_snp", gene:snp) %>%
    dplyr::select(-dplyr::any_of(c(contains("V1"), 
                                   contains("ref"), 
                                   contains("alt"),
                                   contains("beta"),
                                   contains("se"),
                                   contains("chrom_strand")))) %>%
    dplyr::distinct() %>%
    tibble::column_to_rownames("gene_snp")
}



## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 




message("\n ...data frame of features ...")
message(" ")
print(head(output_df %>% dplyr::select(dplyr::any_of(1:10))))
message(" ")





############### Save results ####################

  
message("\n ...Saving results")
  
  
message("... as: ", opt$save)
  
  write.table(output_df,
              sep = "\t",
              paste0(opt$save))
  


## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 



