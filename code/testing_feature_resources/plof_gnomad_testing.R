

################################################################################

# Script for messing around with gnomad constaint data

# pLoF Metrics by Gene TSV (from https://gnomad.broadinstitute.org/downloads#v2-constraint)


################################################################################

# Set up 

library(tidyverse)
library(magrittr)

# load gene wise table

gnomad_table = data.table::fread("/data/gpfs/projects/punim0586/ibeasley/pop_spec_eqtl_ml/data/gnomad/gnomad.v2.1.1.lof_metrics.by_gene.txt")

# load transcript wise table (check is the same as )

gnomad_transcript_table = data.table::fread("/data/gpfs/projects/punim0586/ibeasley/pop_spec_eqtl_ml/data/gnomad/gnomad.v2.1.1.lof_metrics.by_transcript.txt")

# 

all.equal(gnomad_table %>% 
            arrange(gene,transcript) , 
          gnomad_transcript_table %>% 
            filter(canonical == TRUE) %>% 
            select(-canonical) %>% 
            arrange(gene, transcript)) #the ranks of loeuf / and bins are different (no entirely unexpected)

# if we remove rank / bins for loeuf 

all.equal(gnomad_table %>% 
            arrange(gene,transcript) %>% 
            select(-oe_lof_upper_rank,
                   -oe_lof_upper_bin, 
                   -oe_lof_upper_bin_6), 
          gnomad_transcript_table %>% 
            filter(canonical == TRUE) %>% 
            select(-canonical, 
                   -oe_lof_upper_rank,
                   -oe_lof_upper_bin, 
                   -oe_lof_upper_bin_6) %>% 
            arrange(gene, transcript))


# ggplot theme

custom_theme <-
  list(
    # scale_fill_manual(values = friendly_cols,   na.value = "black"),
    # scale_color_manual(values = friendly_cols,   na.value = "black"),
    theme_bw() +
      theme(
        panel.border = element_blank(),
        axis.line = element_line(),
        text = element_text(size = 9),
        legend.position = "bottom",
        strip.background = element_blank(),
        axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)
      )
  )
###############################################################################

# get vector of genes with more than one transcript, to consider whether LOEUF differs in each transcript

genes_with_multi_transcripts = gnomad_table %>% 
  group_by(gene) %>% 
  summarise(n = n()) %>% 
  filter(n > 1) %$% 
  gene

# use this list to extract 

genes_with_multi_transcripts_df = gnomad_table %>% 
                                  filter(gene %in% 
                                           genes_with_multi_transcripts) %>% 
                                          arrange(gene, transcript)

transcript_id = rep(c("A", "B"), 0.5*nrow(genes_with_multi_transcripts_df))

features_of_alt_trans_df  = data.frame(genes_with_multi_transcripts_df, 
           transcript_id = transcript_id) %>% 
  dplyr::select(gene, 
                transcript_id, 
                oe_lof_upper,
                cds_length = cds_length,
                num_coding_exons = num_coding_exons,
                transcript_type = transcript_type,
                transcript_level = transcript_level) %>% 
  tidyr::pivot_wider(., 
                     names_from = transcript_id, 
                     values_from = c(oe_lof_upper, 
                                     cds_length = cds_length,
                                     num_coding_exons = num_coding_exons,
                                     transcript_type = transcript_type,
                                     transcript_level = transcript_level)) 



cor(features_of_alt_trans_df$oe_lof_upper_A, 
    features_of_alt_trans_df$oe_lof_upper_B, 
    use = "na.or.complete")

test = features_of_alt_trans_df %>% 
  filter(num_coding_exons_A > 1 & num_coding_exons_B > 1)

cor(test$oe_lof_upper_A, 
    test$oe_lof_upper_B, 
    use = "na.or.complete")

test = features_of_alt_trans_df %>% 
  filter(num_coding_exons_A == 1 | num_coding_exons_B == 1)

cor(test$oe_lof_upper_A, 
    test$oe_lof_upper_B, 
    use = "na.or.complete")

test = features_of_alt_trans_df %>% 
  filter(num_coding_exons_A == 1 & num_coding_exons_B > 1 | num_coding_exons_A > 1 & num_coding_exons_B == 1)

cor(test$oe_lof_upper_A, 
    test$oe_lof_upper_B, 
    use = "na.or.complete")

 features_of_alt_trans_df %>% 
  ggplot(aes(x=oe_lof_upper_A, 
             y=oe_lof_upper_B, 
             group = gene, 
             name = gene, 
             cds_length_A = cds_length_A,
             cds_length_B = cds_length_B,
             num_coding_exons_A = num_coding_exons_A,
             num_coding_exons_B = num_coding_exons_B,
             transcript_type_A = transcript_type_A,
             transcript_type_B = transcript_type_B,
             transcript_level_A = transcript_level_A,
             transcript_level_B = transcript_level_B,
             )) + 
  geom_abline(slope = 1, intercept = 0, type = 2, col = "red") + 
  geom_point() + 
  xlim(0, 2) + 
  ylim(0, 2) + 
  custom_theme 


 
# Interesting columns for features 
 
 
# Synonymous 
 
# oe_syn: Observed over expected ratio for synoio variants in transcript (obs_syn divided by exp_syn) 
 
# oe_syn_lower: Lower bound of 90% confidence interval for o/e ratio for synonymous variants 
           
syn_table = gnomad_table %>% 
  dplyr::select(gene_id, oe_syn, oe_syn_lower, oe_syn_upper, oe_lof_upper)

cor(syn_table$oe_syn_lower, syn_table$oe_lof_upper, use = "pairwise.complete.obs")

syn_table %>% 
  ggplot(aes(x=oe_lof_upper, oe_syn_upper)) + 
  geom_point() + 
  custom_theme

syn_table %>% 
  ggplot(aes(x=oe_lof_upper, oe_syn_lower)) + 
  geom_point() + 
  custom_theme

syn_table %>% 
  ggplot(aes(x=oe_syn, oe_syn_lower)) + 
  geom_point() + 
  custom_theme

cor(syn_table$oe_syn_lower, syn_table$oe_syn_upper, use = "pairwise.complete.obs")

cor(syn_table$oe_syn, syn_table$oe_syn_upper, use = "pairwise.complete.obs")

cor(syn_table$oe_syn, syn_table$oe_syn_upper, use = "pairwise.complete.obs")


# Missence 

# oe_mis_upper: Upper bound of 90% confidence interval for o/e ratio for missense variants

# oe_mis_pphen: Observed over expected ratio for PolyPhen-2 predicted "probably damaging" missense variants in transcript 


 