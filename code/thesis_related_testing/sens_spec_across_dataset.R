
# sensitivity versus spec for 


# test set 


sens_spec = function(ml_df,
                     test_df){

  
test_list  = readLines(paste0("output/ml_data/ml_test_sets/",test_df))

ml_df = data.table::fread(paste0("ml_runs/", ml_df))

ml_df_test = ml_df %>% 
              filter(ID %in% test_list) %>% 
               group_by(Class,
                              across(any_of(c(contains("Predicted")
                                            )
                                     )
                        )
                        ) %>% 
  count() %>% 
  group_by(Class) %>%
  mutate(freq = n / sum(n)) 


sens = ml_df_test %>% filter(Class == "specific" & across(any_of(contains("Predicted"))) == "specific") %$% freq

spec = ml_df_test %>% filter(Class == "shared" & across(any_of(contains("Predicted"))) == "shared") %$% freq

return(data.frame(sens = sens, spec = spec))

}

list(
     sens_spec("20221031_RF_gs0_n5_mogil_ea_aa/mogil_euro_scores.txt",
               "mogil_ea_spec_aa_v2_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "ea_aa_,snp"),
     sens_spec("20221101_RF_gs0_n5_mogil_ea_aa_mode/mogil_ea_scores.txt",
               "mogil_ea_spec_aa_v2_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "ea_aa_,gene_snp"),
     sens_spec("20221031_RF_gs0_n5_mogil_ea_his/mogil_euro_scores.txt",
               "mogil_ea_spec_his_v2_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "ea_his_,snp"),
     sens_spec("20221101_RF_gs0_n5_mogil_ea_his_mode/mogil_ea_scores.txt",
               "mogil_ea_spec_his_v2_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "ea_his_,gene_snp"),
     sens_spec("20221031_RF_gs0_n5_mogil_aa_ea/mogil_aa_scores.txt",
               "mogil_aa_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "aa_ea_,snp"), 
     sens_spec("20221101_RF_gs0_n5_mogil_aa_ea_mode/mogil_aa_scores.txt",
               "mogil_aa_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "aa_ea_,gene_snp"),
     sens_spec("20221031_RF_gs0_n5_mogil_aa_his/mogil_aa_scores.txt",
               "mogil_aa_spec_his_v2_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "aa_his_,snp"),
     sens_spec("20221101_RF_gs0_n5_mogil_aa_his_mode/mogil_aa_scores_fixed.txt",
               "mogil_aa_spec_his_v2_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "aa_his_,gene_snp"),
     sens_spec("20221101_RF_gs0_n5_mogil_his_ea/mogil_his_scores.txt",
               "mogil_his_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "his_ea_,snp"),
     sens_spec("20221101_RF_gs0_n5_mogil_his_ea_mode/mogil_his_scores.txt",
               "mogil_his_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "his_ea_,gene_snp"),
     sens_spec("20221031_RF_gs0_n5_mogil_his_aa/mogil_his_scores.txt",
               "mogil_his_spec_aa_v2_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "his_aa_,snp"),
     sens_spec("20221101_RF_gs0_n5_mogil_his_ea_mode/mogil_his_scores.txt",
               "mogil_his_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "his_aa_,gene_snp"),
     sens_spec("20221031_RF_gs10_n5_natri_indo_lfsr0.01/natri_indo_scores.txt",
               "natri_indo_spec_v4_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "indo_,snp"),
     sens_spec("20221101_RF_gs0_n5_natri_indo_mode/natri_indo_scores.txt",
               "natri_indo_spec_v4_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "indo_,gene_snp"),
     sens_spec("20221031_RF_gs0_n5_natri_euro/natri_euro_scores.txt",
               "natri_euro_spec_v4_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "euro_,snp"),
     sens_spec("20221101_RF_gs0_n5_natri_euro_mode/natri_euro_scores.txt",
               "natri_euro_spec_v4_lfsr_0.01_majority_rules_features_filtered.txt") %>% 
       mutate(model = "euro_,gene_snp")
) %>% 
  dplyr::bind_rows(.) %>% 
  tidyr::separate(col = "model", into = c("model", "def"), sep = ",") %>% 
  tidyr::pivot_wider(values_from = all_of(c("sens","spec")),
                     names_from = def) %>% 
  mutate(across(!c(model), ~round(.x,digits = 3), .x)) %>% 
  data.table::fwrite("sens_spec_over_datasets_all.csv")



list(
  sens_spec("20221031_RF_gs0_n5_mogil_ea_aa/mogil_euro_scores.txt",
            "mogil_ea_spec_aa_v2_lfsr_0.01_majority_rules_features_filtered.txt") -
    sens_spec("20221101_RF_gs0_n5_mogil_ea_aa_mode/mogil_ea_scores.txt",
            "mogil_ea_spec_aa_v2_lfsr_0.01_majority_rules_features_filtered.txt"),
  sens_spec("20221031_RF_gs0_n5_mogil_ea_his/mogil_euro_scores.txt",
            "mogil_ea_spec_his_v2_lfsr_0.01_majority_rules_features_filtered.txt") -
  sens_spec("20221101_RF_gs0_n5_mogil_ea_his_mode/mogil_ea_scores.txt",
            "mogil_ea_spec_his_v2_lfsr_0.01_majority_rules_features_filtered.txt"),
  sens_spec("20221031_RF_gs0_n5_mogil_aa_ea/mogil_aa_scores.txt",
            "mogil_aa_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt") -
  sens_spec("20221101_RF_gs0_n5_mogil_aa_ea_mode/mogil_aa_scores.txt",
            "mogil_aa_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt"),
  sens_spec("20221031_RF_gs0_n5_mogil_aa_his/mogil_aa_scores.txt",
            "mogil_aa_spec_his_v2_lfsr_0.01_majority_rules_features_filtered.txt") -
  sens_spec("20221101_RF_gs0_n5_mogil_aa_his_mode/mogil_aa_scores_fixed.txt",
            "mogil_aa_spec_his_v2_lfsr_0.01_majority_rules_features_filtered.txt"),
  sens_spec("20221101_RF_gs0_n5_mogil_his_ea/mogil_his_scores.txt",
            "mogil_his_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt") -
  sens_spec("20221101_RF_gs0_n5_mogil_his_ea_mode/mogil_his_scores.txt",
            "mogil_his_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt"),
  sens_spec("20221031_RF_gs0_n5_mogil_his_aa/mogil_his_scores.txt",
            "mogil_his_spec_aa_v2_lfsr_0.01_majority_rules_features_filtered.txt") -
  sens_spec("20221101_RF_gs0_n5_mogil_his_ea_mode/mogil_his_scores.txt",
            "mogil_his_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt"),
  sens_spec("20221031_RF_gs10_n5_natri_indo_lfsr0.01/natri_indo_scores.txt",
            "natri_indo_spec_v4_lfsr_0.01_majority_rules_features_filtered.txt") -
  sens_spec("20221101_RF_gs0_n5_natri_indo_mode/natri_indo_scores.txt",
            "natri_indo_spec_v4_lfsr_0.01_majority_rules_features_filtered.txt"), 
  sens_spec("20221031_RF_gs0_n5_natri_euro/natri_euro_scores.txt",
            "natri_euro_spec_v4_lfsr_0.01_majority_rules_features_filtered.txt") - 
  sens_spec("20221101_RF_gs0_n5_natri_euro_mode/natri_euro_scores.txt",
            "natri_euro_spec_v4_lfsr_0.01_majority_rules_features_filtered.txt")
) %>% 
  dplyr::bind_rows(.) %>% 
  mutate(sens = -1*sens,
         spec = -1*spec) %>% 
  summarise(across(everything(), ~median(.x)))


list(
  sens_spec("20221031_RF_gs0_n5_mogil_ea_aa/mogil_euro_scores.txt",
            "mogil_ea_spec_aa_v2_lfsr_0.01_majority_rules_features_filtered.txt") -
    sens_spec("20221101_RF_gs0_n5_mogil_ea_aa_mode/mogil_ea_scores.txt",
              "mogil_ea_spec_aa_v2_lfsr_0.01_majority_rules_features_filtered.txt"),
  sens_spec("20221031_RF_gs0_n5_mogil_ea_his/mogil_euro_scores.txt",
            "mogil_ea_spec_his_v2_lfsr_0.01_majority_rules_features_filtered.txt") -
    sens_spec("20221101_RF_gs0_n5_mogil_ea_his_mode/mogil_ea_scores.txt",
              "mogil_ea_spec_his_v2_lfsr_0.01_majority_rules_features_filtered.txt"),
  sens_spec("20221031_RF_gs0_n5_mogil_aa_ea/mogil_aa_scores.txt",
            "mogil_aa_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt") -
    sens_spec("20221101_RF_gs0_n5_mogil_aa_ea_mode/mogil_aa_scores.txt",
              "mogil_aa_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt"),
  sens_spec("20221031_RF_gs0_n5_mogil_aa_his/mogil_aa_scores.txt",
            "mogil_aa_spec_his_v2_lfsr_0.01_majority_rules_features_filtered.txt") -
    sens_spec("20221101_RF_gs0_n5_mogil_aa_his_mode/mogil_aa_scores_fixed.txt",
              "mogil_aa_spec_his_v2_lfsr_0.01_majority_rules_features_filtered.txt"),
  sens_spec("20221101_RF_gs0_n5_mogil_his_ea/mogil_his_scores.txt",
            "mogil_his_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt") -
    sens_spec("20221101_RF_gs0_n5_mogil_his_ea_mode/mogil_his_scores.txt",
              "mogil_his_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt"),
  sens_spec("20221031_RF_gs0_n5_mogil_his_aa/mogil_his_scores.txt",
            "mogil_his_spec_aa_v2_lfsr_0.01_majority_rules_features_filtered.txt") -
    sens_spec("20221101_RF_gs0_n5_mogil_his_ea_mode/mogil_his_scores.txt",
              "mogil_his_spec_ea_v2_lfsr_0.01_majority_rules_features_filtered.txt"),
  sens_spec("20221031_RF_gs10_n5_natri_indo_lfsr0.01/natri_indo_scores.txt",
            "natri_indo_spec_v4_lfsr_0.01_majority_rules_features_filtered.txt") -
    sens_spec("20221101_RF_gs0_n5_natri_indo_mode/natri_indo_scores.txt",
              "natri_indo_spec_v4_lfsr_0.01_majority_rules_features_filtered.txt"), 
  sens_spec("20221031_RF_gs0_n5_natri_euro/natri_euro_scores.txt",
            "natri_euro_spec_v4_lfsr_0.01_majority_rules_features_filtered.txt") - 
    sens_spec("20221101_RF_gs0_n5_natri_euro_mode/natri_euro_scores.txt",
              "natri_euro_spec_v4_lfsr_0.01_majority_rules_features_filtered.txt")
) %>% 
  dplyr::bind_rows(.) %>% 
  mutate(sens = -1*sens,
         spec = -1*spec) %>% 
  mutate(across(everything(), ~round(.x,digits = 3))) %>% 
  data.table::fwrite("sens_spec_over_datasets.csv")


#   filter(ID %in% test_aa_ea) %>% 
#   filter(Class == "specific") %>% 
#   group_by(Predicted_0.5) %>% 
#   summarise(n =n())
# 
# }
# 
# natri_indo_0_scores %>% 
#   filter(ID %in% test_set) %>% 
#   group_by(Class,
#            across(any_of(c(contains("Predicted")
#                            )
#                          )
#                   )
#            ) %>% 
#   count() %>% 
#   group_by(Class) %>%
#   mutate(freq = n / sum(n)) 
