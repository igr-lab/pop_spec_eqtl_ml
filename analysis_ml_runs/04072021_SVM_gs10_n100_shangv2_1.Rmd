---
title: "04072021_SVM_gs10_n100_shangv2_1"
site: workflowr::wflow_site
output:
  workflowr::wflow_html:
    toc: false
editor_options:
  chunk_output_type: console
---

Performing a bit worse than Random Forest than I’d expected (my expectations could be off though), may have something to with this warming: 
  
  ConvergenceWarning: Liblinear failed to converge, increase the number of iterations.

Also, once again getting warming: 
  
  ML_functions.py:712: RuntimeWarning: invalid value encountered in long_scalars
precis.append(TP / (TP + FP))

