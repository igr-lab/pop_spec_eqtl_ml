
################################################################################


# Brief integration of results

imp_table = data.table::fread("./pop_spec_eqtl_ml/ml_runs/01062021_RF_gs0_n10_S/metab_RF_imp")

imp_table = read.table("./pop_spec_eqtl_ml/ml_runs/01062021_RF_gs0_n10_S/metab_RF_results.txt")

###############################################################################

# Looking at what importance the model gives particular features

imp_table = data.frame(importance, 
                       rank = 1:nrow(importance)) %>% # give each feature a rank in terms of importance
             rowwise() %>% # group features into categories for plotting (these categories are GO terms + mashr output
             mutate( # Gene conservation + SNP Conservation + Allele Freq and other
                     Group = ifelse(grepl("GO", V1), "GOSlim Term", 
                                   ifelse(grepl("beta", V1) | grepl("se", V1), 
                                          "mashr output (effect size / se)",
                                          ifelse("oe_lof_upper" == V1 | "phastcons"== V1 | "phylop" == V1,
                                                 "Gene Conservation / Constraint",
                                          ifelse("fitCons" == V1 | grepl("cadd", V1) | "linsight" == V1, 
                                                 "SNP Conservation / Consequence",
                                          ifelse(grepl("af", V1), "Allele Freq", 
                                                "Other")))))
                                                              )  # Other includes %GC window in SNP, Distance of SNP to TSS


             imp_table %>% 
            rowwise() %>% 
               mutate(scaled_mean_imp = mean_imp/0.270) %>% 
             ggplot() + 
             geom_col(aes(x=rank, y=scaled_mean_imp, fill = Group)) + 
             custom_theme + 
            scale_fill_brewer(palette="Set2") + 
             labs(x = "Features ranked by mean importance",
                  y= "Mean importance") 
            # theme(fill = brewer.pal(unique(imp_table$Group) %>% length(), "Dark2"))
             
 
               test_2 %>% 
               ggplot() + 
               geom_histogram(aes(x=Mean, fill = Class), bins = 100) + 
               facet_wrap(~Class, nrow =2) + labs(x = "Mean Predicted 'Specificity' Score", y = "") + 
               custom_theme +    
               scale_fill_brewer(palette="Dark2") + 
                 geom_vline(xintercept = 0.49) + 
                 geom_text(data = data.frame(x = 0.55, y = 20000, Class = "shared", label = paste0(round(anno_shared*100, digits = 2), "%")), 
                           aes(x = x, y = y, label = label))  + 
                 geom_text(data = data.frame(x = 0.55, y =20000, Class = "specific", label = paste0(round(anno_spec*100, digits = 2), "%")), 
                           aes(x = x, y = y, label = label)) + 
                 geom_text(data = data.frame(x = 0.45, y = 20000, Class = "shared", label = paste0(round(100-anno_shared*100, digits = 2), "%")), 
                           aes(x = x, y = y, label = label))  + 
                 geom_text(data = data.frame(x = 0.45, y =20000, Class = "specific", label = paste0(round(100-anno_spec*100, digits = 2), "%")), 
                           aes(x = x, y = y, label = label))
               
              geom_text(mapping = aes(x = 0.7, y = 4),  label = paste0(round(anno_shared*100, digits = 4), "%"))
                # 
                #           
                # 
                # x = 0.7, y = 5, label = paste0(round(anno_shared*100, digits = 4), "%"
                #  
                #  annotate("text", x = 0.7, y = 5, label = paste0(round(anno_shared*100, digits = 4), "%"))
                # 
               
               

  