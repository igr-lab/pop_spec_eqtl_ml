

#merge features df with preclassified eqtls


#feat_df
feat_df = read.table("./output/ml_data/ml_test_sets/natri_indo_ml_test_mod_v2.1_filtered.txt")

class_eqtl_df = read.table("./output/classified_eqtls/natri_indo_classified_0.8_factor_eff", header = TRUE)

class_eqtl_df[1:5,1:5]

class_eqtl_df = class_eqtl_df %>% tidyr::unite(., "gene_snp", gene:snp) %>% tibble::column_to_rownames("gene_snp")

class_eqtl_df = class_eqtl_df %>% tibble::rownames_to_column("gene_snp")

feat_df = feat_df %>%  tibble::rownames_to_column("gene_snp")

updated_df = inner_join(class_eqtl_df %>% select(-ref,-alt),
           feat_df %>% select(-Class))

feat_df %>% group_by(Class) %>% summarise(n = n())

updated_df %>% group_by(Class) %>% summarise(n = n())


updated_df %>% tibble::column_to_rownames("gene_snp") %>% 
  write.table("output/ml_data/ml_test_sets/natri_indo_lfsrv3_0.8_factor_eff.txt",
              sep = "\t") 
