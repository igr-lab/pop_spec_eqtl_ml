
# Figure 5. Violin Plots of Specific and Shared Features

setwd(here::here())


################################################

library(dplyr)
library(ggplot2)
library(ggpubr)

############ Load relevant data: ####################

# Indonesian-specific ml data frame
natri_indo_ml = data.table::fread("output/ml_data/ml_full_sets/natri_indo_spec_v4_lfsr_0.01_majority_rules_features_filtered.txt")

# GTEx (European-specific) ml data frame

gtex_eqtls = data.table::fread("output/ml_data/ml_full_sets/natri_euro_spec_v4_lfsr_0.01_majority_rules_features_filtered.txt")

# Mogil European American

ea_his_eqtls_med = data.table::fread("./output/ml_data/ml_full_sets/mogil_ea_spec_his_v2_lfsr_0.01_majority_rules_features.txt")

ea_aa_eqtls = data.table::fread("./output/classified_eqtls/mogil_ea_spec_aa_v2_lfsr_0.01_majority_rules_features_med.txt")

ea = inner_join(ea_his_eqtls %>% rename(Class_1 = Class),
                ea_aa_eqtls %>% rename(Class_2 = Class)) %>% 
                mutate(Class = case_when(Class_1 == Class_2 & Class_1 == "specific" ~ "non-portable",
                                         Class_1 == Class_2 & Class_1 == "shared"  ~ "portable",
                                         Class_1 == "specific" ~ "portable (EA - AA only)",
                                         TRUE~ "portable (EA to HIS only)"))
                                     
     #                                 
     #                                 
     #                                 ifelse(Class == "shared", "portable (EA to HIS)", "not-portable")),
     #       ea_aa_eqtls %>% mutate(Class_2 = ifelse(Class == "shared", "portable (EA to AA)", "not-portable"))) %>% 
     # mutate(Class = case_when(Class_1 == "portable (EA to HIS)" & Class_2 == "portable ()"))

# Mogil African American 

aa_his_eqtls = data.table::fread("./output/classified_eqtls/mogil_aa_spec_his_v2_lfsr_0.01_majority_rules_features.txt")

aa_ea_eqtls = data.table::fread("./output/classified_eqtls/mogil_aa_spec_ea_v2_lfsr_0.01_majority_rules_features.txt")

# Mogil Hispanic

his_ea_eqtls = data.table::fread("./output/classified_eqtls/mogil_his_spec_ea_v2_lfsr_0.01_majority_rules_features.txt")

his_aa_eqtls = data.table::fread("./output/classified_eqtls/mogil_his_spec_aa_v2_lfsr_0.01_majority_rules_features.txt")


his_plot = inner_join(his_ea_eqtls %>% 
           rename(his_ea_class = Class),
           his_aa_eqtls %>% 
           rename(his_aa_class = Class)) %>% 
  mutate(Class = case_when(his_ea_class != "shared" & his_aa_class != "shared" ~ "non-portable",
                           his_ea_class != "shared" ~ "non-portable (HIS to AA)",
                           his_aa_class != "shared" ~ "non-portable (HIS to EA)",
                           TRUE ~ "portable"))


########### Set up packages #####################

# Get function for permutation test for the difference in two means: 
options(box.path = here::here())
box::use(code/utils/perm_means_test[...])

########## Plot settings ############

{
  
hispanic_col = c("#009988")
shared_col = c("#000000")
european_col = c("#0077BB")
indonesian_col = c("#CC3311")
african_american_col = c("#EE3377")
na_col = c("#888888")

col_pal_natri = c(shared_col,
                  indonesian_col,
                  european_col,
                  na_col)

col_pal_mogil = c(shared_col,
                  shared_col,
                  shared_col,
                  african_american_col)

col_pal_mogil_his = c(shared_col,
                     shared_col,
                     shared_col,
                     hispanic_col)

col_pal_mogil_ea = c(shared_col,
                     shared_col,
                     shared_col,
                      european_col)

# inspired by vibrant palette from 
# khroma R package
# orange      blue      cyan   magenta       red      teal      grey 
# "#EE7733" "#0077BB" "#33BBEE" "#EE3377" "#CC3311" "#009988" "#BBBBBB"
}

######### Violin plot with permutation p-value figure function ##########


ggviolin_fig5_style = function(ml_df,
                               y,  #Feature to violin plot
                               ylim = NULL, 
                               x = "Class",
                               n_pval_perm = 100000, #number of p-value permutations
                               trait_type,
                               add = "mean_sd",  #"median_iqr",
                               ID_col = "V1",
                               ylab = y,
                               palette,
                               ...){
  
  
  ml_df = ml_df %>% as.data.frame()
  
  box::use(dplyr[...],
           rlang[...],
           ggpubr[...])
  
  if(trait_type == "gene"|
     trait_type == "protein"){
    
    ml_df = ml_df %>% 
            tidyr::separate(ID_col, 
                            into = c("gene", "snp")) %>% 
            group_by(gene) %>% 
            slice_sample(n = 1) %>% 
            ungroup()
    
  }
  
  # create list of all pairwise combinations of classes:
  
  pairwise_combs = combn(unique(ml_df$Class), 2, simplify = FALSE)
  
  # for each pairwise comb, calculate p-values
  
  group1 = vector()
  group2 = vector()
  p = vector()
  
  for(comb in pairwise_combs){
    
      filt_df = ml_df %>% dplyr::filter(!!sym(x) %in% comb) %>% as.data.frame()
      feature_vec = filt_df %>% dplyr::pull(y)
      class_vec = filt_df %>% dplyr::pull(x)
      
      p_value = permutation_mean_test(feature = feature_vec,
                                      class = class_vec,
                                      n = n_pval_perm)
      
      group1 = append(group1, comb[1])
      group2 = append(group2, comb[2])
      p = append(p, p_value)
      
  }
  
  
  # calculate where to plot these pvalues on the y-axis
  
  ymax = max(ml_df %>% dplyr::pull(y), na.rm = TRUE)
  
  n_compare = length(pairwise_combs)
  
  y.position = seq(to = 0.675*ymax,
                   from = 1.025*ymax,
                   length.out = n_compare)
  
  # create pvalue dataframe for plotting: 
  
  p_value_df = data.frame(group1 = group1,
                          group2 = group2,
                          p = p,
                          y.position = y.position) %>% 
              mutate(p = ifelse(p == 0, 
                                paste0("p < ", 1/n_pval_perm),
                                 paste0("p = ", p)))
 
  
  plot_violin = ml_df %>% 
    ggviolin(y= y, 
             ylim = ylim, 
             x = x,
             fill = x,
             trim = TRUE,
             ylab = ylab,
             palette = palette,
             add = add,
             size = 0.1,
             xlab = FALSE,
             font.label = list(size = 300, 
                               color = "black"),
             add.params = list(fill = "white", 
                               color= "white", 
                               size = 0.1),
             ...) +
    labs(x = NULL) + # https://rpkgs.datanovia.com/ggpubr/reference/stat_pvalue_manual.html
    stat_pvalue_manual(p_value_df,
                       #label = p_label, #"p = {p}",
                       size = 3,
                       label.size = 3,
                       bracket.shorten = 0.1,
                       tip.length = 0.02)
  
  Fig  = ggpar(plot_violin,
               font.x = 8,
               font.y = 8,
               xlab = FALSE,
               font.label = 8,
               font.legend = 8,
               font.tickslab = 8,
               legend = "none"
               )
  
  return(Fig)
  
}



######### Set up plot page ########

{
  
  library(ggpubr)
  library(ggplot2)
  library(plotgardener)
  
  pdf(file = "thesis_figures/violin_natri.pdf",
      width = 6.25, 
      height = 8.75,
      family = "Helvetica",
      paper = "a4")
  
  pageCreate(width = 158.75, 
             height = 222.25, 
             xgrid = 5,
             ygrid = 5,
             #showGuides = FALSE,
             default.units = "mm")

##########  Figure 5A. Gene Conservation Scores ############

  plotText(label = "A",
           fontsize = 12,
           x = 0.5,
           y = 2.5,
           just = "left",
           default.units = "mm",
           fontface = "bold",
           fontfamily="Helvetica")

  Fig_5A = ggviolin_fig5_style(natri_indo_ml,
                               y= "oe_lof_upper",
                               ylim = c(0,2.15), 
                               ylab = "LOEUF",
                               trait_type = "gene",
                               palette = col_pal_natri)
  plotGG(plot = Fig_5A,
         x = 3,
         y = 4.5,
         width = 50,
         height = 55,
         just = c("left", "top"),
         default.units = "mm")

######### Figure 5B. SNP Conservation Scores  ##########

  plotText(label = "B",
           fontsize = 12,
           x = 75,
           y = 2.5,
           just = "left",
           default.units = "mm",
           fontface = "bold")

  Fig_5B = ggviolin_fig5_style(natri_indo_ml,
                               y = "fitCons",
                               ylim = c(0,1),
                               trait_type = "snp",
                               palette = col_pal_natri)

plotGG(plot = Fig_5B,
       x = 76.5,
       y = 4.5,
       width = 50,
       height = 55,
       just = c("left", "top"),
       default.units = "mm")

######### Figure 5C. LD Scores ###########

plotText(label = "C",
         fontsize = 12,
         x = 0.5,
         y = 62,
         just = "left",
         default.units = "mm",
         fontface = "bold")


Fig_5C = ggviolin_fig5_style(natri_indo_ml, 
                             y= "mean_LDScore_reduced",
                             ylab = "Mean LD Score",
                             trait_type = "snp",
                             palette = col_pal_natri) 


plotGG(plot = Fig_5C,
       x = 3,
       y = 64.5,
       width = 60,
       height = 55,
       just = c("left", "top"),
       default.units = "mm")

######## Figure 5D. Variance in LD Score ########

plotText(label = "D",
         fontsize = 12,
         x = 75,
         y = 62,
         just = "left",
         default.units = "mm",
         fontface = "bold")


Fig_5D = ggviolin_fig5_style(natri_indo_ml %>% 
                               mutate(var_LDScore_reduced = var_LDScore_reduced + 1) %>% 
                               as.data.frame(),
                             y= "var_LDScore_reduced",
                             ylab = "Variance of LD Score",
                             ylim = c(0,1e+06),
                             trait_type = "snp",
                             palette = col_pal_natri,
                             yscale = "log2"
                             ) #+ 


         #scale_y_log10(expand=c(0.01,0))


ggviolin_fig5_style(his_plot %>% filter(!is.na(var_LDScore_reduced)) %>% 
                      mutate(var_LDScore_reduced = log(var_LDScore_reduced)), 
                    y= "var_LDScore_reduced",
                    ylab = "Var LD Score",
                    trait_type = "snp",
                    palette = col_pal_mogil_his) 

plotGG(plot = Fig_5D,
       x = 76.5,
       y = 64.5,
       width = 60,
       height = 55,
       just = c("left", "top"),
       default.units = "mm")

######## Figure 5D. Allele frequency ##########

# Fig_5A = ggviolin_fig5_style(natri_indo_ml,
#                              y= "indo_maf",
#                              ylim = c(0,1), 
#                              ylab = "Indonesian Allele Frequency",
#                              palette = col_pal_natri)

pageGuideHide()

dev.off()

}

