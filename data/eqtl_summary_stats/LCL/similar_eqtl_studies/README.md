
# Column information for files in Shang_2020/similar_eqtl_studies 

*** 

<br> </br> 

## (1) README For GEUVADIS (non-eQTL catalogue) files (EUR373 and YRI89)

- Joint README file for all eQTL, quantification and genotype files of Lappalainen et al. Nature 2013. 
- Taken from: [https://www.ebi.ac.uk/arrayexpress/files/E-GEUV-1/GeuvadisRNASeqAnalysisFiles_README.txt](https://www.ebi.ac.uk/arrayexpress/files/E-GEUV-1/GeuvadisRNASeqAnalysisFiles_README.txt)


Please see GeuvadisRNASeqAnalysisFiles.xlsx from https://www.ebi.ac.uk/arrayexpress/files/E-GEUV-1/analysis_results/ for a summary of eQTL and quantification files. Further details of the file contents and formats are provided below. Description of the analysis methods can be found in the Supplementary methods. 



-----------------------------------------------------------------------

eQTL file set:
- Sample set & sample size : EUR373, YRI89
- Quantitative trait: exon, gene, transcript ratio (trratio), transcribed repetitive element (repeat), miRNA (mi)
- Set of associations included in the file: All the associations below false discovery rate 5%, or best association per each gene (for exon, transcript ratio) or unit (gene, repeat, miRNA). If there are several best associating variants with the same p-value, one of them has been chosen randomly. 

eQTL file format: 
- The file contains variantÃ¢â‚¬â€œQT association information, with the following columns:
1	SNP_ID : Variant identifier according to dbSNP137; position-based identifier for variants that are not in dbSNP (see Supplementary material pp 45) 
2	ID : Null (-)
3	GENE_ID : Gene identifier according to Gencode v12, miRBase v18, repeats based on their start site
4	PROBE_ID : Quantitative trait identifier; the same as GENE_ID expect for:
		Exons: GENEID_ExonStartPosition_ExonEndPosition
		Transcript ratios: Transcript identifier according to Gencode v12
5	CHR_SNP : Chromosome of the variant
6	CHR_GENE : Chromosome of the quantitative trait
7	SNPpos : Position of the variant
8	TSSpos : Transcription start site of the gene/QT
9	Distance : | SNPpos Ã¢â‚¬â€œ TSSpos | 
10	rvalue	: Spearman rank correlation rho (calculated from linear regression slope). The sign denotes the direction of the nonreference allele (i.e. rvalue<0 means that nonreference allele has lower expression) 
11	pvalue : Association p-value
12	log10pvalue : -log10 of pvalue

-----------------------------------------------------------------------

Quantification file set:
- Sample set + sample size : 
QC-passed: All QC-passed samples including replicates: 660 (mRNA) or 480 (miRNA)
QC-passed unique: Nonredundant set of unique samples used in most analyses: 462 (mRNA); 452 (miRNA)
- Normalization: 
	None: raw read counts
	Library depth: Read counts scaled by total number of mapped reads (mRNA), or total number reads mapping to miRNAs (miRNA) per sample, then adjusted to the median of the sample set (45M for mRNA, 1.2M for miRNA)
	Library depth and transcript length: RPKM
	Library depth & expressed & PEER: Library depth scaling as above, removal of units with 0 counts in >50% samples, and removal of technical variation by PEER normalization

Quantification file format: 
- The files provide quantification unit (rows) x samples (columns) tables. The header line gives sample IDs, and the four first rows provide information of the quantification unit: 
1	TargetID : Quantitative trait identifier; the same as GENE_ID expect for:
		Exons: GENEID_ExonStartPosition_ExonEndPosition
		Transcript ratios: Transcript identifier according to Gencode v12
2	Gene_Symbol : Gene identifier according to Gencode v12, miRBase v18, repeats based on their start site
3	Chr : Chromosome
4	Coord : Start site of the element (taking strand into account)
- November 5, 2013 update: The file GD462.GeneQuantRPKM.50FN.samplename.resk10.norm.txt.gz that had the normalization as above PLUS an additional transformation of each gene's values to standard normal has been replaced by GD462.GeneQuantRPKM.50FN.samplename.resk10.txt.gz

-----------------------------------------------------------------------

Genotype file set:
- Genotype files are split into chromosomes, and the sites file contains all sites without genotype data. The files include all samples included in RNA-sequencing. The splice score file contains variants that overlap splice sites, their predicted splicing scores, and genotypes. 
- The annotation information relies on Gencode v12 gene annotation, and regulatory annotation includes only LCL annotations from Ensembl regulatory build. For details, see Supplementary Methods and http://sanabre.net/geuvadis/index.php/Variant_annotation .
- October 7, 2013 update: (1) we have fixed an error where 42 imputed samples appeared to have phased data even though the data was in fact unphased. (2) The files also now include annotations. (3) We have added a file to convert Geuvadis-type IDs to dbSBP v137 IDs (that didn't exist when we started analyzing the data). (4) ALL.phase1_release_v3.20101123.snps_indels_sv.sites.gdid.gdannot.v2.vcf.gz is has our annotations for all the 1000 Genomes variants (rather than only those that are polymorphic in our samples). 

Genotype file formats:
- Genotype and sites files are in VCF 4.1 format (https://github.com/amarcket/vcf_spec) files
- For details of the annotations in the INFO field, see http://sanabre.net/geuvadis/index.php/Variant_annotation .  For coding variants, SEVERE_IMPACT and SEVERE_GENE are key fields with the most severe impact of this variant across all affected transcripts and genes. 
- Splice score file follows the general principles of the vcf format, with the following columns
1	CHROM : chromosome
2	POS : the position of the splice site, provided as the first/last position included in the adjacent exon (cf. AStalavista default coordinates)
3	ID : string identifying the splice site uniquely, composed by strand, genomic coordinate, site type symbol (cf. AStalavista conventions) and chromosome ID
4	The reference sequence of the splice site, as obtained by extracting the corresponding sequence stretch from the genome
5	Comma-separated list of the corresponding splice site sequences after applying the corresponding genetic variant(s) to the reference sequence
6	QUAL : missing
7	"q-1000" when the splice site sequence has not observed in the training set (-Infinity, in practice represented by a value << -1000), "PASS" otherwise
8	INFO : MOD: either alternative (ALT) or constitutive (CON) splice site; ALTx :(combinations of) variants that form each alternative variant (same ordering as in ALT column); REF_SCORE: splice scores assigned to the reference sequence; VAR_SCORES: comma-separated list of scores assigned to the corresponding variant(s); the ordering corresponds to the one used for ALT; SNPS: concatenation of all variants considered for the description of this splice site

*** 

<br> </br> 

# (2) For TwinsUK / GTEx /   Information on column data for eQTL summary statistics taken from the eQTL catalogue

***

- Files taken from https://github.com/eQTL-Catalogue/eQTL-Catalogue-resources/blob/master/tabix/tabix_ftp_paths.tsv
- Information taken directly from:[https://github.com/eQTL-Catalogue/eQTL-Catalogue-resources/blob/master/tabix/Columns.md](https://github.com/eQTL-Catalogue/eQTL-Catalogue-resources/blob/master/tabix/Columns.md)

# Column names of the nominal eQTL summary statistics files (*.all.tsv.gz)

* **variant** - The variant ID (chromosome_position_ref_alt) e.g. chr19_226776_C_T. Based on GRCh38 coordinates and reference genome. The chromosome, position, ref and alt values should exactly match same fields in the summary statistics file, with 'chr' prefix added to the chromosome number. 
* **r2** - Optional imputation quality score from the imputation software, can be replaced with NA if not available.
* **pvalue** - Nominal p-value of association between the variant and the molecular trait.
* **molecular_trait_object_id** - For phenotypes with multiple correlated alternatives (multiple alternative transcripts or exons within a gene, multple alternative promoters in txrevise, multiple alternative intons in Leafcutter), this defines the level at which the phenotypes were aggregated. Permutation p-values are calculated across this set of alternatives.  
* **molecular_trait_id** - ID of the molecular trait used for QTL mapping. Depending on the quantification method used, this can be either a gene id, exon id, transcript id or a txrevise promoter, splicing or 3'end event id. Examples: ENST00000356937, ENSG00000008128.  
* **maf** - Minor allele frequency within a QTL mapping context (e.g. cell type or tissues within a study).
* **gene_id** - Ensembl gene ID of the molecular trait. 
* **median_tpm** - Median transcripts per million (TPM) expression value of the gene. Can be replaced with NA if not available (e.g. in microarray  studies).
* **beta** - Regression coefficient from the linear model.
* **se** - Standard error of the beta.
* **an** - Total number of alleles. For autosomal variants, this is usually two times the sample size. Conversely, for autosomal variants, sample size is equal to an/2.
* **ac** - Count of the alternative allele. 
* **chromosome** - GRCh38 chromosome name of the variant (e.g. 1,2,3 ...,X).
* **position** - GRCh38 position of the variant.
* **ref** - GRCh38 reference allele.
* **alt** - GRCh38 alternative allele (also the effect allele).
* **type** - Type of the genetic variant; SNP, INDEL or OTHER.
* **rsid** - The dbSNP v151 rsid of the variant. If the same variant has multiple rsids then these should be split over multiple rows so that all of the other values are duplicated.

# Column names of the permutation p-value files (*.permuted.tsv.gz)

* **molecular_trait_object_id** - For phenotypes with multiple correlated alternatives (multiple alternative transcripts or exons within a gene, multple alternative promoters in txrevise, multiple alternative intons in Leafcutter), this defines the level at which the phenotypes were aggregated. Permutation p-values are calculated accross this set of alternatives.
* **molecular_trait_id** - ID of the molecular trait used for QTL mapping. Depending on the quantification method used, this can be either a gene id, exon id, transcript id or a txrevise promoter, splicing or 3'end event id. Examples: ENST00000356937, ENSG00000008128. 
* **n_traits** - The number of molecular traits over which permutation p-values were calculated (e.g. the number of transcripts per gene). Note that the permutations are performed accross all molecular traits within the same molecular trait object (e.g. all transcripts of a gene) and the results are reported for the most significant variant and molecular trait pair. 
* **n_variants** - number of genetic variants tested within the cis region of the molecular trait.
* **variant** - The variant ID (chromosome_position_ref_alt) e.g. chr19_226776_C_T. Based on GRCh38 coordinates and reference genome. The chromosome, position, ref and alt values should exactly match same fields in the summary statistics file, with 'chr' prefix added to the chromosome number.
* **chromosome** - GRCh38 chromosome name of the variant (e.g. 1,2,3 ...,X).
* **position** - GRCh38 position of the variant.
* **pvalue** - Nominal p-value of association between the variant and the molecular trait.
* **beta** - Regression coefficient from the linear model.
* **p_perm** - Empirical p-value calculated from 1000 permutations.
* **p_beta** - Estimated empirical p-value based on the beta distribution. This is the column that you want to use for filtering the results. See the FastQTL [paper](http://dx.doi.org/10.1093/bioinformatics/btv722) for more details. 

# Column names of the fine mapping credible set files from SuSiE

* **molecular_trait_id** - ID of the molecular trait used for QTL mapping. Depending on the quantification method used, this can be either a gene id, exon id, transcript id or a txrevise promoter, splicing or 3'end event id. Examples: ENST00000356937, ENSG00000008128.
* **variant** - The variant ID (chromosome_position_ref_alt) e.g. chr19_226776_C_T. Based on GRCh38 coordinates and reference genome. The chromosome, position, ref and alt values should exactly match same fields in the summary statistics file, with 'chr' prefix added to the chromosome number.  
* **chromosome** - GRCh38 chromosome name of the variant (e.g. 1,2,3 ...,X).
* **position** - GRCh38 position of the variant.
* **ref** - GRCh38 reference allele.
* **alt** - GRCh38 alternative allele (also the effect allele).
* **cs_id** - unique ID for each credible set within a dataset
* **cs_index** - credible set id for each phenotype
* **finemapped_region** - start and end coordinates of the fine mapped region
* **pip** - posterior inclusion probability the variant
* **z** - univariate z-score for the variant
* **cs_min_r2** - minimal LD (r2) between any two variants in the credible set
* **cs_avg_r2** - average LD (r2) between pairs of variants within a credible set
* **cs_size** - credible set size 
* **posterior_mean**  
* **posterior_sd**
* **cs_log10bf**
